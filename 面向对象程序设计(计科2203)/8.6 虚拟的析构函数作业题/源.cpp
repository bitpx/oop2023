#include<iostream>
using namespace std;
class A;//A类的原型声明
A* dataArray[1001];
int len = 0;

//-------------------code here--------------------
class A
{
public:
	virtual ~A();
};
class B :public A
{
public:
	~B()
	{
		cout << "del B\n";
		dataArray[++len] = new A();
	}
};

A::~A()
{
	if (len % 2)
	{
		cout << "del A\n";
		dataArray[++len] = new B();
	}
}

//----------------------------------------------------
int main()
{
	dataArray[++len] = new A;
	int n; cin >> n;
	for (int i = 1; i <= n; i++) delete(dataArray[i]);
}