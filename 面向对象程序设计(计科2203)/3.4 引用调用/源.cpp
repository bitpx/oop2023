#include<iostream>
#define N 5
using namespace std;

// 传值调用
void addOne(int x)
{
	x++;
}

/*
   传地址调用：
   1.形参是指针变量
   2.形参是数组(形参数组名实际上作为指针变量使用)
   结果：对形参的操作直接影响到实际参数
*/
void addOne(int* x, int array[])
{
	(*x)++;
	for (int i = 0; i < N; i++)
	{
		array[i]++;
	}
}


/*
   引用调用：
   1.写法，在传值调用基础上，只需要在形参前加上&符号
   2.结果和传地址调用一致
*/
void AnotherAddOne(int& x)
{
	x++;
}

int main()
{
	int a = 2, ints[N] = { 1,2,3,4,5 };
	addOne(a);
	addOne(&a, ints);
	AnotherAddOne(a);

	return 1;
}