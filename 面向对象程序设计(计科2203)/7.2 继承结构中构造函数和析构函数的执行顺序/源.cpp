#include<iostream>
using namespace std;

class GrandFather {
private:
	int data;
public:
	void show() {
		cout << data << endl;
	}
	GrandFather()
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
	~GrandFather()
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
};
//单继承、私有继承
class Father :virtual public  GrandFather {
public:
	Father()
	{
		cout << "Father对象诞生了！" << endl;
	}
	~Father()
	{
		cout << "Father对象消亡了！" << endl;
	}
};
//单继承、私有继承
class Mother :virtual public  GrandFather {
public:
	Mother()
	{
		cout << "Mother对象诞生了！" << endl;
	}
	~Mother()
	{
		cout << "Mother对象消亡了！" << endl;
	}
};
//多继承、公有继承
class Son :public Father, public Mother {
public:
	Son()
	{
		cout << "Son对象诞生了！" << endl;
	}
	~Son() {
		cout << "Son对象消亡了！" << endl;
	}
};


void main()
{
	{
		Son son;
		son.Mother::show();
		//使用
	}
	Son* s = new Son;
	s->Father::show();
	//使用
	delete s;
}