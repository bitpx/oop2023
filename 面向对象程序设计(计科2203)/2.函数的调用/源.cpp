#include<iostream>
using namespace std;

//传值调用
void f1(int a, int b)
{
	a++;
	b++;
	cout << a << "," << b << "," << endl;
}
//指针变量作为形参，传地址调用
void f2(int* a, int* b)
{
	(*a)++;
	(*b)++;
	cout << *a << "," << *b << "," << endl;
}
//引用调用
void f3(int& a, int& b)
{
	a++;
	b++;
	cout << a << "," << b << "," << endl;
}

void main()
{
	int a = 1, b = 2;
	f1(a, b);//传值调用
	f2(&a, &b);//传地址调用
	f3(a, b);//引用调用
	cout << a << "," << b << "," << endl;
}