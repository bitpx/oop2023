#include<iostream>
using namespace std;

/*
	一个函数，使用关键字inline(内联)声明
	作用：在编译阶段，函数调用语句会被具体的最终执行代码片段替换掉
	条件：
	1.函数不能包含递归调用语句
	2.函数简单、没有复杂的业务逻辑
*/
// 业务逻辑
// 声明为内联函数
inline int add(int x, int y)
{
	return x + y;
}

void main()
{
	int a = 1, b = 2, sum1, sum2;
	// 业务逻辑
	sum1 = add(a, b);
	sum2 = add(3, 4);
	// 界面逻辑
	cout << sum1 << " " << sum2 << endl;
}