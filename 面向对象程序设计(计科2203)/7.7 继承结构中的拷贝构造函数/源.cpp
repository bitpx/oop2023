#include<iostream>
using namespace std;

class Parent
{
private:
	int pri;
public:
	Parent(const Parent& o)
	{
		this->pri = o.pri;
	}
	//其他函数省略
};
class Son :public Parent
{
public:
	Son(const Son& o)
		:Parent(o) //类型兼容规则的典型应用
	{

	}
};

void main()
{

}