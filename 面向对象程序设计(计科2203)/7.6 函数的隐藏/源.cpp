#include <iostream>
using namespace  std;

class Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Parent(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	//被隐藏
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
	//被隐藏
	void show(int x)
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
	//被隐藏
	virtual void show(int x, int y)
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
	//被覆盖
	virtual void show(int x, int y, int z)
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};
class Son :public Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Son(int pri, int pro, int pub)
		:Parent(pri + 2, pro + 2, pub + 2)
	{
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	//1.覆盖了父类型中的相同函数(虚函数，相同即同名同参同返回值类型)
	//2.隐藏了其他的同名函数
	void show(int x, int y, int z)
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};

int main() {

	Son son(1, 2, 3);
	son.Parent::show();//被隐藏了
	son.Parent::show(1);//被隐藏了
	son.Parent::show(1, 2);//被隐藏了

	son.show(1, 2, 3);//访问子类型自己的版本

	return 0;
}
