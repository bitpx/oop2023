#include<iostream>
using namespace std;

class Complex {
private:
	double x, y;
	/*friend Complex& operator+(const Complex& a, const Complex& b);
	friend Complex& operator-(const Complex& a, const Complex& b);*/
	friend Complex& operator++(Complex& c);
	friend Complex& operator++(Complex& c, int);
public:
	Complex(double x, double y)
		:x(x), y(y)
	{
	}
	double getX() const { return x; }
	double getY() const { return y; }
	Complex& operator+(const Complex& b)
	{
		Complex c(this->x + b.x, this->y + b.y);
		return c;
	}
	Complex& operator-(const Complex& b)
	{
		Complex c(x - b.x, y - b.y);
		return c;
	}
	//前置--
	Complex& operator--()
	{
		this->x -= 1;
		this->y -= 1;
		return *this;
	}
	//后置--
	Complex& operator--(int)
	{
		Complex old(this->x, y);
		/*c.x += 1;
		c.y += 1;*/
		--(*this);//调用前置++重载函数
		return old;
	}
};
////将运算符号+进行了非成员函数的重载
//Complex& operator+(const Complex& a, const Complex& b)
//{
//	Complex c(a.x + b.x, a.y + b.y);
//	return c;
//}
////将运算符号-进行了非成员函数的重载
//Complex& operator-(const Complex& a, const Complex& b)
//{
//	Complex c(a.x - b.x, a.y - b.y);
//	return c;
//}

ostream& operator<<(ostream& out, const Complex& c)
{
	out << "(" << c.getX() << "," << c.getY() << ")" << endl;
	return out;
}

//前置++
Complex& operator++(Complex& c)
{
	c.x += 1;
	c.y += 1;
	return c;
}
//后置++
Complex& operator++(Complex& c, int)
{
	Complex old(c.x, c.y);
	/*c.x += 1;
	c.y += 1;*/
	++c;//调用前置++重载函数
	return old;
}

void main()
{
	Complex a(1.0, 2.0), b(2.0, 3.0);
	Complex c = a + b;
	cout << c;
	Complex d = a - b;
	cout << d;
	Complex e = ++a;
	Complex f = b++;
	Complex g = --e;
	Complex h = f--;
}