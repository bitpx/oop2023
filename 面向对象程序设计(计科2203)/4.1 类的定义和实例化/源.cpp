#include<iostream>
using namespace std;

// 时钟
/*
   数据抽象：hour,minute,second
   行为抽象：set函数，get函数，其他函数
*/

class Tools {
public:
	// 静态成员函数，属于类，不属于对象
	static void ClockDataCheckAndAssign(int& virable, int value,
		int maxVaue = 59,
		int minValue = 0)
	{
		if (value <= maxVaue && value >= minValue)
			virable = value;
		else
		{
			virable = 0;
		}
	}
};


class Point {
private:
	float x, y;
public:
	Point(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
	Point()
	{
		/*this->x = 0.0f;
		this->y = 0.0f;*/
		Point(0.0f, 0.0f);
	}
};


class Clock {
private:
	int hour, minute, second;
protected:
public:
	// 构造函数在对象诞生时给对象数据成员初始化
	Clock(int hour, int minute, int second)
		:hour(hour), second(second)//初始化列表
	{
		/*this->hour = hour;
		  this->second = second;
		*/
		this->minute = minute;
	}
	// 先诞生对象后给对象数据成员赋值
	Clock()
	{
	}
	void setHour(int hour = 0) {
		Tools::ClockDataCheckAndAssign(this->hour, hour, 23);
	}
	void setMinute(int minute) {
		Tools::ClockDataCheckAndAssign(this->minute, minute);
	}
	void setSecond(int second = 0) {
		Tools::ClockDataCheckAndAssign(this->second, second);
	}
	int getHour() { return this->hour; }
	int getMinute() { return minute; }
	int getSecond() { return second; }
};

void main()
{
	Point* p1 = new Point;


	// 将类实例化为对象,真正的对象，存储于栈中
	Clock c1(12, 15, 30);
	Clock c3;
	c3.setHour(14);
	// 先构建对象空间，后赋值
	c1.setHour(13);
	c1.setMinute(80);
	c1.setSecond(100);
	cout << c1.getHour() << ":"
		<< c1.getMinute()
		<< ":" << c1.getSecond() << endl;

	// 另外一种方式实例化对象
	Clock* c2;// 并非真正的对象，只是1个指针变量，存储于栈中
	//c2 = (Clock*)malloc(sizeof(Clock));
	c2 = new Clock(20, 30, 45);//将类实例化为1个对象(使用new来分配空间)，堆区
	Clock* c4 = new Clock();// new让系统将类实例化为对象，然后系统会自动去选择对应的构造函数执行
	// 先构建对象空间，后赋值
	c2->setHour(13);
	c2->setMinute(80);
	c2->setSecond(100);
	cout << c2->getHour() << ":"
		<< c2->getMinute()
		<< ":" << c2->getSecond() << endl;
}