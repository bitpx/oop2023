#include<iostream>
using namespace std;

class Student {
private:
	string no;
	//常数据成员，必须在声明时初始化，必须在初始化列表初始化
	const string name/* = "dsx"*/;
public:
	Student(string no, string name) :name(name)
	{
		this->no = no;
	}
	void setNo(string no) {
		this->no = no;
	}
	//常函数，只能读取数据
	string  getNo()  const
	{
		return this->no;
	}
	//普通的函数
	string  getNo()
	{
		return this->no;
	}
};
void main()
{
	//const关键字，“常”--“只读”(不能修改)
	//s1为普通对象
	Student s1("1001", "a");
	//普通对象可以调用普通的set函数
	s1.setNo("1002");
	//普通对象默认调用普通函数，但是也可以调用唯一的常函数
	cout << s1.getNo() << endl;

	//s2为常对象，常对象不能修改数据，只能读取
	const Student s2("1003", "b");
	//常对象只能调用常函数
	//s2.setNo("1004");
	//常函数
	cout << s2.getNo() << endl;

	//声明一个常引用s3，
	const Student& s3 = s1;
	//常引用只能调用常函数，不能修改数据，只能读取
	cout << s3.getNo() << endl;
	//s3.setNo("1005");只能读取，不能修改
}