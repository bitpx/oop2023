#include<iostream>
using namespace std;

class Student {
private:
	string no;
	//声明友元函数
	friend void displayNo(Student stu);
	//声明友元类，友元类中所有函数都能访问当前类私有段成员
	friend class PrintForNo;
public:
	Student(string no)//实例级别的成员
	{
		this->no = no;
	}
	void setNo(string no) {//实例级别的成员
		this->no = no;
	}
	string getNo()//实例级别的成员
	{
		return this->no;
	}
};

void displayNo(Student stu)
{
	//可直接访问对象的私有段成员
	cout << stu.no << endl;
}

class PrintForNo {
private:
	Student stu;
public:
	PrintForNo(string no) :stu(no)
	{
	}
	void print()
	{
		cout << stu.no << endl;
	}
	void display()
	{
		cout << stu.no << endl;
	}
};


void main()
{

}