#include<iostream>
using namespace std;

class A {
private:
	const int data;
public:
	A(int x)
		:data(x)
	{
	}
};
class B {
public:
	B(int y) {}
};
class C//整体类----类的组合
{
private:
	A a;//局部类
	B b;//局部类
public:
	C(int x, int y)
		:a(x), b(y)
	{

	}
};




class GrandFather {
private:
	int data;
public:
	void show() {
		cout << data << endl;
	}
	GrandFather(int x)
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
	~GrandFather()
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
};
//单继承、私有继承
class Father :virtual public  GrandFather {
public:
	Father(int x)
		:GrandFather(x)
	{
		cout << "Father对象诞生了！" << endl;
	}
	~Father()
	{
		cout << "Father对象消亡了！" << endl;
	}
};
//单继承、私有继承
class Mother :virtual public  GrandFather {
public:
	Mother(int x)
		:GrandFather(x)
	{
		cout << "Mother对象诞生了！" << endl;
	}
	~Mother()
	{
		cout << "Mother对象消亡了！" << endl;
	}
};
//多继承、公有继承
class Son :public Father, public Mother {
public:
	Son(int x)
		:Father(x), Mother(x), GrandFather(x)
	{
		cout << "Son对象诞生了！" << endl;
	}
	~Son() {
		cout << "Son对象消亡了！" << endl;
	}
};


void main()
{
	{
		Son son(1);
		son.show();
		//使用
	}
	Son* s = new Son(1);
	s->show();
	//使用
	delete s;
}