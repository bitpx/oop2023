#include<iostream>
using namespace std;

class GrandFather {

};
//单继承、私有继承
class Father :private GrandFather {

};
//单继承、保护继承
class Monther :protected  GrandFather {

};
//多继承、公有继承
class Son :public Father, public Monther {

};
//基类型(父类型，一般类)
class Base
{
private:
	int pri;
protected:
	int pro;
public:
	Base() {}
	~Base() {}
	int pub;
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};
//私有继承：将父类型的保护段和公有段继承下来放置于子类型的私有段
class Derived1 :private Base {
private:
	int x;
public:
	void f1() {
		//访问私有段的成员
		cout << pro << pub /*<< pri*/ << endl; show();
	}
};
//保护继承：将父类型的保护段和公有段继承下来放置于子类型的保护段
class Derived2 :protected Base {
public:
	void f2() {
		//访问保护段的成员 
		show();
		cout << pro << pub /*<< pri*/ << endl;
	}
};
//公有继承：将父类型的保护段和公有段继承下来放置于子类型的保护段
class Derived3 :public Base {
};


void main()
{
	Derived1 o1;
	o1.f1();
	Derived2 o2;
	o2.f2();
	Derived3 o3;
	//cout << o3.pro << endl;
	cout << o3.pub << endl;
	o3.show();
}