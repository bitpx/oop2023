#include<iostream>
#include<string>
using namespace std;

class A {
private:
	string name;
	//1.构造函数要放置于私有段，杜绝外界的随意访问
	A(string name)
	{
		this->name = name;
	}
	//2.静态的指针变量，指向唯一的对象
	static A* obj;
public:
	//3.静态的成员函数，获取唯一的对象
	static A* GetObject(string name)
	{
		if (A::obj == NULL)
		{
			A::obj = new A(name);
		}
		return obj;
	}
	void Print()
	{
		cout << this->name << endl;
	}
};

//声明加上初始化
A* A::obj = NULL;

int main() {

	string name1, name2;
	cin >> name1;
	cin >> name2;

	A* o1 = A::GetObject(name1);
	o1->Print();


	A* o2 = A::GetObject(name2);
	o2->Print();

	if (o1 == o2)
	{
		cout << "o1与o2指向的是同一个对象！" << endl;
	}

	delete o1;

	return 0;
}