#include<iostream>
using namespace std;

//类类型--数据类型
//将数据及对数据的操作放在一起形成一个不可分离的整体
class Calculator {
private://私有段--一般放数据
	int a;
	int b;
public://公有段--一般放操作
	void setValue(int x, int y) {
		a = x;
		b = y;
	}
	int getSum() {
		return a + b;
	}
	int getSub() {
		return a - b;
	}
};

class Student {
private:
	string no, name;
	short age;//8-60岁限制
public:
	void setAge(short age) {
		if (age < 8 || age>60)
			return;
		this->age = age;
	}
	void setVaue(string _no, string name, short age)
	{
		no = _no;
		this->name = name;
		this->age = age;
	}
	void studying()
	{
		cout << name << "正在学习！" << endl;
	}
	void eating()
	{
		cout << name << "正在吃饭！" << endl;
	}
};

void main()
{
	Student s1;//诞生(实例化)了一个学生对象
	/*s1.age = 1000;*/
	s1.setAge(1000);//1个消息



	Calculator c;//c更应称为对象
	int sum, sub;//两个变量

	/*
	  c.a = 1;
	  c.b = 2;
	*/
	c.setValue(1, 2);
	sum = c.getSum();
	sub = c.getSub();
	cout << sum << "  " << sub << endl;

	Calculator d;//又诞生一个对象d
	d.setValue(4, 5);
	sum = d.getSum();
	sub = d.getSub();
	cout << sum << "  " << sub << endl;




	int a, b;
	cin >> a >> b;
	int sum = a + b;




