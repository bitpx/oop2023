#include<iostream>
using namespace std;

void main()
{
	//申请整型变量空间
	int* p1 = (int*)malloc(sizeof(int));
	int* p2 = new int;//在堆中申请一个存储整数的空间
	int* p3 = new int(10);//在堆中申请一个存储整数的空间，初始化值为10
	free(p1); delete(p2); delete(p3);

	//申请一维数组空间
	int* p4 = new int[3];
	delete[] p4;
	int* p5 = new int[3] {1, 2, 3};
	delete[] p5;

	//申请二维数组空间
	int ints[2][3];//存储于栈中
	int(*p6)[9] = new int[2][9];

	//申请三维数组空间
	int arrays[2][3][4];//存储于栈中
	int(*p7)[3][4] = new int[2][3][4];

	//指针数组
	int* myarray[5];//存储于栈中
	int** p = myarray;
	p = new int* [5];//存储于堆中
}