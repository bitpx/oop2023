#include<iostream>
using namespace std;

/*
   注意：有缺省值的参数必须放置于无缺省值参数的右边
*/

int getSum(int a = 1, int b = 2,
	int c = 0/*=0，即c的缺省值*/,
	int d = 0/*=0，即d的默认值*/)
{
	return a + b + c + d;
}

/*
  C++中函数重载的代码写法
*/
//int getSum(int a, int b) {
//	return a + b;
//}

// 求三个整数的和
//float getSum(int a, int b, int c) {
//	return a + b + c;
//}

// 求四个整数的和
//int getSum(int a, int b, int c, int d)
//{
//	return a + b + c + d;
//}


///* 以下代码是C语言的函数写法 */
//// 求两个整数的和
//int getSumTwoInts(int a, int b) {
//	return a + b;
//}
//
//// 求三个整数的和
//int getSumThreeInts(int a, int b, int c) {
//	return a + b + c;
//}
//
//// 求四个整数的和
//int getSumFourInts(int a, int b, int c, int d)
//{
//	return a + b + c + d;
//}

void main()
{

	int a = 1, b = 2, c = 3, d = 4;
	int sum;
	sum = getSum(a, b);
	sum = getSum(a, b, c);
	sum = getSum(a, b, c, d);

}