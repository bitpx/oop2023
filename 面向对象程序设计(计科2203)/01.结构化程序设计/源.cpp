/*
功能：两个整数的加法和减法
被操作的数据：两个整数，a与b
操作：减法、加法
*/
#include<stdio.h>
/*
* 操作1:执行加法运算
*/
int getSum(int x, int y) {
	return x + y;
}
/*
* 操作2:执行减法运算
*/
int getSub(int x, int y) {
	return x - y;
}

void  main()
{
	//被操作的数据
	int a, b;

	int sum, sub;

	//界面逻辑代码--变化比较大
	scanf_s("%d%d", &a, &b);

	//业务逻辑
	/*sum = a + b;
	sub = a - b;*/



	/*sum = getSum(a, b);
	sub = getSub(a, b);*/

	//界面逻辑代码
	printf("%d+%d=%d", a, b, sum);
	printf("%d-%d=%d", a, b, sub);

}