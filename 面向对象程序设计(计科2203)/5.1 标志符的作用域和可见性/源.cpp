#include<iostream> //<>指示直接在系统自带的库文件夹中搜索代码
#include "Tools.h" //先在用户文件夹中搜索代码，如果不存在，再到系统自带的库文件夹中搜索代码
using namespace std;
using namespace ToolsNS;

//存储于静态数据区(全局数据区)
//全局变量，也是一个命名空间变量
//声明了一个全局变量，默认的最大的命名空间（系统在编译阶段自动添加）的全局变量
int i = 0;//作用域从当前声明行开始至整个源代码结束

void f1()
{
	Tools1 o1;
	Tools2 o2;
	i = 1;//全局变量i
}
// 界面逻辑的命名空间
namespace UINS {
	class UI1 {

	};
	class UI2 {

	};
}


void f2()
{
	//存储栈区
	//i为局部变量
	int i = 1;//与前方的全局变量i同名，覆盖全局变量i在当前函数的作用域
	cout << i << endl;//i为当前函数的局部变量i
}
namespace NS {
	namespace NS2 {
		//.....
	}
	int j = 1;//存储于静态数据区
	void f3()
	{
		cout << j;
	}
	class Point {
	public:
		Point(float x, float y)
		{
			this->x = x;
			this->y = y;
		}
	private:
		float x;//作用域为当前整个类范围
		float y;//作用域为当前整个类范围
	};
}

void f4()
{
	int x = 0;
	switch (x)
	{
	case 0:
	{
		int i = 0;
		break;
	}
	case 1:
	{
		int i = 1;
		break;
	}
	default:
		break;
	}
	//int i = 1;
	////使用i

	//int i = 1;
	////使用i
}



void main()
{
	using namespace NS;
	i = 5;//全局变量i
	int j = 100;//局部变量，与命名空间NS中的j(全局变量)同名
	int x = 1, y = 5, sum;//内部变量

	cout << j << endl;//输出的值是局部变量j的值，为100
	cout << NS::j << endl;//输出的值是全局变量j的值，为1

	NS::j = 6;//使用命名空间全局变量j
	int add(int a = 0, int y = 0);//函数的原型声明
	//a = 0;

	//代码块
	{
		using namespace NS;
		int i;//定义了一个局部变量(块级别)，覆盖了前面的全局变量i
		int j = 10;
		i = 7;
		cout << "i=" << i << endl;
		cout << "j=" << j << endl;
		cout << "j=" << NS::j << endl;//加前缀的原因是NS中的全局变量的可见性被同名的局部变量隐藏掉
	}//代码块中的局部变量i到此结束

	sum = add(x, y);
	cout << "i=" << i << endl;//全局变量i
}

//x、y是两个形参，是两个局部变量，作用域仅限函数头和整个函数体
int add(int x = 0, int y = 0)
{
	//内部变量，作用域仅限当前行至函数体结束
	int z = 5; cout << z << endl;
	//int x = 12;//同级别作用域中，不能声明同名的变量
	x++;
	for (int i = 0; i < 15; i++)//i为块级别变量，作用域仅限当前代码块
	{
		cout << i << endl;
	}
	//以下的for循环与上面的for循环属于不同的代码块，可使用相同的变量名
	for (int i = 0; i < 15; i++)//i为块级别变量，作用域仅限当前代码块
	{
		cout << i << endl;
	}
	return x + y + z;
}



