#include<iostream>
using namespace std;

class Parent {
public:
	Parent()
	{}
	virtual ~Parent()
	{
		cout << "父类对象消亡了！" << endl;
	}
};

class Son :public Parent {
public:
	Son()
	{}
	~Son()
	{
		cout << "子类对象消亡了！" << endl;
	}
};


void main()
{
	Parent* p = new Son();
	//使用中
	delete p;
}