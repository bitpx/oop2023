#include<iostream>
using namespace std;

class Base {
private:
	int pri;
protected:
	int pro;
public:
	Base(const Base& o);
	Base(int pri, int pro, int pub);
	int pub;
	void show();
};

Base::Base(const Base& o) {
	this->pri = o.pri;
	this->pro = o.pro;
	this->pub = o.pub;
}
Base::Base(int pri, int pro, int pub)
	:pri(pri), pro(pro), pub(pub)
{
}
inline void Base::show() {
	cout << pri << "" << pro << "" << pub << endl;
}

class Derived :public Base
{
public:
	Derived(int x, int y, int z)
		:Base(x, y, z)
	{}
	void print()
	{
		cout << "------------------------" << endl;
		show();
		cout << "------------------------" << endl;
	}
};

void main()
{
	//Base p1(1, 2, 3);
	//p1.show();

	//Derived s(1, 2, 3);
	////将子类型对象隐含地转变为父类型对象
	//Base p2 = s;
	//p2.show();

	//父类型指针可以指向子类型对象
	Base* p3 = new Derived(1, 2, 3);
	p3->show();
	/*p3->print();*/

	//父类型的引用可以指向子类型对象
	Derived son(1, 2, 3);
	Base& p4 = son;
	p4.show();
	son.print();
	/*p4.print();*/

}