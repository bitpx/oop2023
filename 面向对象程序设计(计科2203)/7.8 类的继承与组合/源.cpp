#include<iostream>
using namespace std;

class GrandFather
{

};

class Father
{
private:
	int pri;
public:
	Father(int pri)
		:pri(pri)
	{
		cout << "Father对象诞生了" << endl;
	}
	~Father()
	{
		cout << "Father对象消亡了" << endl;
	}
	//其他函数省略
};
class Mother
{
private:
	int pri;
public:
	Mother(int pri)
		:pri(pri)
	{
		cout << "Mother对象诞生了" << endl;
	}
	~Mother()
	{
		cout << "Mother对象消亡了" << endl;
	}
	//其他函数省略
};
//编号类
class No {
private:
	string value;
public:
	No(string value) :value(value)
	{
		cout << "No对象诞生了" << endl;
	}
	~No()
	{
		cout << "No对象消亡了" << endl;
	}
};
class Name {
private:
	string value;
public:
	Name(string value) :value(value)
	{
		cout << "Name对象诞生了" << endl;
	}
	~Name()
	{
		cout << "Name对象消亡了" << endl;
	}
};
class Son :public Father, public  Mother
{
private:
	No no;
	Name name;
public:
	Son(int pri, string no, string name)
		:Mother(pri), Father(pri), no(no), name(name)
	{
		cout << "Son对象诞生了" << endl;
	}
	~Son()
	{
		cout << "Son对象消亡了" << endl;
	}
};

void main()
{
	Son son(1, "1001", "aa");
}