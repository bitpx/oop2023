#include<iostream>
using namespace std;

class Hour {//局部类
private:
	int data;
public:
	Hour(int data) {
		this->data = data;
		cout << "部分类Hour对象诞生了！" << endl;
	}
	~Hour() { cout << "部分类Hour对象空间释放了！" << endl; }
};
class Minute {//局部类
private:
	int data;
public:
	Minute(int data) {
		this->data = data;
		cout << "部分类Minute对象诞生了！" << endl;
	}
	~Minute() {
		cout << "部分类Minute对象空间释放了！" << endl;
	}
};
class Second {//局部类
private:
	int data;
public:
	Second(int data) {
		this->data = data;
		cout << "部分类Second对象诞生了！" << endl;
	}
	~Second()
	{
		cout << "部分类Second对象空间释放了！" << endl;
	}
};

class Clock {//整体类
private:
	//局部类对象作为数据成员
	Hour hour;
	Minute minute;
	Second second;
	int h, m, s;//基础类型成员
protected:
public:
	~Clock()
	{
		cout << "整体类对象释放了！" << endl;
	}
	Clock(int h, int m, int s)
		//只能通过初始化列表初始化对象成员
		: second(s), hour(h), minute(m), h(h), s(s)
	{//一旦进入构造函数表，表示整体类对象诞生了
		this->m = m;
		cout << "整体类对象诞生了！" << endl;
	}
	Clock() :hour(0), minute(0), second(0)
	{
		cout << "整体类对象诞生了！" << endl;
	}
};

void main()
{
	Clock* c = new Clock(12,13,14);
	// 使用对象....
	delete c;
}