#include<iostream>
using namespace std;

/*
   函数的重载(充分必要条件)：
   1.几个函数使用相同的函数名
   2.相同函数名的几个函数形参的个数或类型不一样
   3.可通过不同类型的形参顺序实现函数重载(无意义)
   无关：
   1.与形参的名称无关
   2.与函数返回值类型无关
*/

/*
   比较两个数的大小，返回最大的一个数
   通过不同类型的形参的不同顺序形参函数重载(不推荐)
*/
float getMax(int a, float b) {
	return a > b ? a : b;
}
float getMax(float b, int a) {
	return a > b ? a : b;
}


/*
  C++中函数重载的代码写法
*/
int getSum(int a, int b) {
	return a + b;
}

// 求三个整数的和
float getSum(int a, int b, int c) {
	return a + b + c;
}

// 求四个整数的和
int getSum(int a, int b, int c, int d)
{
	return a + b + c + d;
}


/* 以下代码是C语言的函数写法 */
// 求两个整数的和
int getSumTwoInts(int a, int b) {
	return a + b;
}

// 求三个整数的和
int getSumThreeInts(int a, int b, int c) {
	return a + b + c;
}

// 求四个整数的和
int getSumFourInts(int a, int b, int c, int d)
{
	return a + b + c + d;
}

void main()
{
	int x = 10;
	float y = 1.2f;
	float r;
	r = getMax(x, y);
	r = getMax(y, x);



	int a = 1, b = 2, c = 3, d = 4;
	int sum;
	sum = getSumTwoInts(a, b);
	sum = getSumThreeInts(a, b, c);
	sum = getSumFourInts(a, b, c, d);

	sum = getSum(1, 2);
	sum = getSum(1, 2, 3);
	sum = getSum(1, 2, 3, 4);

}