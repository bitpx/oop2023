//统计学生对象的个数
#include<iostream>
#include<string.h>
using namespace std;

class Point {
	float x, y;
public:
	Point(float x, float y)
	{//....
	}
	//Point() {}
};

class Student {
private:
	//静态特征(属于对象)，不能说成静态成员
	string no;//实例级别的成员，每个实例(对象)都有一个副本
	//静态(static的中文翻译)数据成员
	static int n;//类级别的成员，属于类，整个类中只有一个副本，可供所有对象共享
public:
	//静态(类级别)成员函数一般只访问静态的数据成员
	static int getN(/*Student o*/)//类级别的成员
	{
		/*cout << o.no << endl;*/
		//cout << this->n << endl;//类级别的函数不需要this，因为此函数不属于任何对象
		return Student::n;
	}

	Student(string no)//实例级别的成员
	{
		this->no = no;
		//this->n++;
		Student::n++;
	}
	void setNo(string no) {//实例级别的成员
		this->no = no;
	}
	string getNo()//实例级别的成员
	{
		return this->no;
	}
};

//类似于函数原型声明
int Student::n = 0;

void main()
{
	Student s1("1001");
	Student s2("1002");
	Student s3("1003");
	cout << "学生对象的数量为:" << Student::getN() << endl;
	//表示某个对象正在调用getN方法，不推荐
	cout << "学生对象的数量为:" << s1.getN() << endl;
	cout << "学生对象的数量为:" << s2.getN() << endl;
	cout << "学生对象的数量为:" << s3.getN() << endl;

	//缺陷：
	/*
	  1.n++写了多次，相同的代码重复的编写
	  2.n是统计学生的数量，但是与student类毫无关系
	*/
	/*int n = 0;
	Student s1("1001");
	n++;
	Student s2("1002");
	n++;
	Student s3("1003");
	n++;
	cout << "学生对象的数量为:" << n << endl;*/

}