#include<iostream>
using namespace std;

//抽象类，用来规约子类型必须要实现功能是哪些
class Role {
public:
	virtual void attack() = 0;
};
class A
{
public:
	void attacking()
	{
		cout << "使用飞刀攻击" << endl;
	}
};
class B
{
public:
	void shooting()
	{
		cout << "使用枪射击" << endl;
	}
};
//通过类的组合实现功能复用
class MrLiXunHuan :public Role
{
private:
	A a;
public:
	void attack()
	{
		//复用成员对象a的攻击功能
		a.attacking();
	}
};
//通过继承实现功能复用
class MrLiYunLong :public Role,public B
{
public:
	void attack()
	{
		//复用父类型B中实现的攻击功能
		B::shooting();
	}
};

void main()
{
	Role* r;
	//李寻欢使用飞刀攻击
	r = new MrLiXunHuan();
	r->attack();
	delete r;

	//李云龙使用枪射击
	r = new MrLiYunLong();
	r->attack();
	delete r;
}