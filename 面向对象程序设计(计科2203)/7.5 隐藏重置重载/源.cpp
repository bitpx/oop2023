#include <iostream>
using namespace  std;

class Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Parent(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	virtual void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};
class Son :public Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Son(int pri, int pro, int pub)
		:Parent(pri + 2, pro + 2, pub + 2)
	{
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};

int main() {
	//父类型的指针指向子类型对象--类型兼容规则
	Parent* p = new Son(1, 2, 3);
	p->show();//目标是调用子类型的版本

	return 0;
}
