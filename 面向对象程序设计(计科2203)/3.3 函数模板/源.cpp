#include<iostream>
using namespace std;

/*
  比较两个数的大小，返回较大的一个数
*/
//int getMax(int x, int y)
//{
//	return x > y ? x : y;
//}
//float getMax(float x, float y)
//{
//	return x > y ? x : y;
//}
//double getMax(double x, double y)
//{
//	return x > y ? x : y;
//}


template <class T> //T类型变元，T是Type单词的缩写
T getMax(T x, T y)
{
	return x > y ? x : y;
}

void main()
{
	int m1;
	double m2;
	m1 = getMax(1, 2);
	m2 = getMax(1.1, 2.3);
	/*printf("%.1lf", m2);*/
}