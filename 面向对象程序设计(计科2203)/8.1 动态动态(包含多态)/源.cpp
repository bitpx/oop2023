#include<iostream>
using namespace std;

//求图形的面积和周长
//父类型：放置所有子类型都共有的成员
//包含纯虚函数(抽象函数)的类称为抽象类
//抽象类不能被实例化为对象
class Shape {
private:
	string name;
public:
	Shape(string name)
	{
		this->name = name;
	}
	//求面积
	//纯虚函数-抽象函数
	virtual double  getArea() = 0;

	//求周长
	//纯虚函数-抽象函数
	virtual double  getPerimeter() = 0;

	string getName() const { return name; }
	void setName(string name) { this->name = name; }
};

class Rectangle :public Shape
{
private:
	double length;
	double width;
public:
	Rectangle(string name, double length, double width)
		:Shape(name), length(length), width(width)
	{
	}
	//求面积
	double  getArea()
	{
		return length * width;
	}
	//求周长
	double  getPerimeter()
	{
		return 2 * (length + width);
	}
};

class Square :public Shape
{
private:
	double width;
public:
	Square(string name, double width)
		:Shape(name), width(width)
	{
	}
	//求面积
	double  getArea()
	{
		return width * width;
	}
	//求周长
	double  getPerimeter()
	{
		return 4 * width;
	}
};

void main()
{
	//求长方形面积和周长-父类型的指针指向子类型的对象
	Shape* s = new Rectangle("长方形", 5.0, 4.0);
	double area = s->getArea();
	double peri = s->getPerimeter();
	cout << area << endl;
	cout << peri << endl;

	//求正方形面积和周长
	s = new Square("正方形", 5.0);
	area = s->getArea();
	peri = s->getPerimeter();
	cout << area << endl;
	cout << peri << endl;


	////求长方形面积和周长
	//Rectangle* r = new Rectangle("长方形", 5.0, 4.0);
	//double area = r->getArea();
	//double peri = r->getPerimeter();
	//cout << area << endl;
	//cout << peri << endl;

	////求正方形面积和周长
	//Square* s = new Square("正方形", 5.0);
	//area = s->getArea();
	//peri = s->getPerimeter();
	//cout << area << endl;
	//cout << peri << endl;
}