#include<iostream>
using namespace std;

class Point {
public:
	Point(const Point& obj)//obj为一个常(只读)引用
	{
		//进入构造函数体，全新对象诞生了，
		//通过this可找到刚刚新诞生的全新对象
		this->x = obj.x;
		this->y = obj.y;
		/*obj.x = 1.6f;
		obj.setX(1.6);*/
	}
	//常规的构造函数
	Point(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
	void setX(float x) { this->x = x; }
	void setY(float y) { this->y = y; }
	float getX() { return x; }
	float getY() { return y; }
private:
	float x;
private:
	float y;
};

void f1(Point p)
{
	cout << p.getX() << " " << p.getY() << endl;
}

Point f2()
{
	//局部对象，在函数执行结束时，对象空间会自动释放
	Point p(1.2f, 2.4f);
	//在p消亡前，执行拷贝构造函数诞生一个新对象，将新对象返回
	return p;//返回的对象实际上是拷贝构造函数诞生的全新对象，非p对象
}

void main()
{
	Point p1(1.2f, 2.4f);
	//1.将现有对象作为参数实例化全新的对象
	Point p2(p1);//当对象作为构造函数的实际参数-调用拷贝构造函数
	//2.将现有对象赋值给一个全新的对象
	Point p3 = p2;
	//3.当现有对象作为一个函数的形参，进行实参和形参相结合时
	f1(p3);
	//4.当一个函数返回数据为对象时，拷贝构造函数也会执行
	Point p4 = f2();
}