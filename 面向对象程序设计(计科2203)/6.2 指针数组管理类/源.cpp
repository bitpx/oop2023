#include<iostream>
using namespace std;

class MyArray {
private:
	int** p;//指向指针数组的指针
	int rowCount;//行数
	int columnCount;//列数
public:
	MyArray(int rowCount, int columnCount)
		:rowCount(rowCount), columnCount(columnCount)
	{
		p = new int* [rowCount];
		int v = 1;
		for (int i = 0; i < rowCount; i++)
		{
			p[i] = new int[columnCount];
		}
		for (int i = 0; i < rowCount; i++)
		{
			for (int j = 0; j < columnCount; j++)
			{
				p[i][j] = v++;
			}
		}
	}
	~MyArray() {
		//释放存储具体数据的一维数组空间
		for (int i = 0; i < rowCount; i++)
		{
			delete[] p[i];
		}
		delete[] p;
	}
	int getRowCount() const
	{
		return this->rowCount;
	}
	int getColumnCount() const
	{
		return this->columnCount;
	}
	int getData(int i, int j) const
	{
		return p[i][j];
	}
};

void main()
{
	MyArray* o = new  MyArray(4, 5);
	for (int i = 0; i < o->getRowCount(); i++)
	{
		for (int j = 0; j < o->getColumnCount(); j++)
		{
			cout << o->getData(i, j) << " ";
		}
		cout << endl;
	}

}