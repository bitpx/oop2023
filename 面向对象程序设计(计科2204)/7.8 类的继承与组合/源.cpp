#include<iostream>
using namespace std;

class Father {
public:
	Father(int x)
	{
		cout << "Father 构造函数执行了" << endl;
	}
	~Father()
	{
		cout << "Father 析构函数消亡了" << endl;
	}
};
class Mother {
public:
	Mother(int x)
	{
		cout << "Mother 构造函数执行了" << endl;
	}
	~Mother()
	{
		cout << "Mother 析构函数消亡了" << endl;
	}
};

class No
{
private:
	string value;
public:
	No(string value) {
		this->value = value;
		cout << "No构造函数诞生了" << endl;
	}
	~No() {
		cout << "No析构函数消亡了" << endl;
	}
	//其他函数省略
};

class Name
{
private:
	string value;
public:
	Name(string value) {
		this->value = value;
		cout << "Name构造函数诞生了" << endl;
	}
	~Name() {
		cout << "Name析构函数消亡了" << endl;
	}
	//其他函数省略
};

class Son :public Father, public Mother {
private:
	No no;
	Name name;
public:
	Son(int x, string no, string name)
		:Father(x), Mother(x), no(no), name(name)
	{
		cout << "Son 构造函数执行了" << endl;
	}
	~Son()
	{
		cout << "Son 析构函数消亡了" << endl;
	}
};

void main()
{
	Son son(1, "123", "邓少勋");
}