#include<iostream>
using namespace std;

/*
  带缺省(默认)参数值的函数：
  1.允许形参有一个默认值
  注意事项：有缺省值的参数必须放置于无缺省值参数的后面
*/

int getSum(int a, int b, int c = 0, int d = 0)
{
	return a + b + c + d;
}


void main()
{
	int x = 1, y = 2, z = 3, e = 4;
	int sum;
	sum = getSum(x, y);
	sum = getSum(x, y, z);
	sum = getSum(x, y, z, e);
}