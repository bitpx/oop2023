#include<iostream>
using namespace std;

class Point
{
private:
	float x, y;
public:
	//拷贝构造函数,const为“常”，只读，通过常引用只能对对象执行只读操作
	Point(const Point& obj)//形参为一个引用，是一个常引用
	{
		this->x = obj.x;//当前类的成员函数体中可执行访问任意权限段的成员
		this->y = obj.y;
		/*obj.x = 2.3f;
		obj.setValue(1.2f, 3.5f);*/
	}
	//常规的构造函数
	Point(float x, float y) {
		this->x = x; this->y = y;
	}
	void setValue(float x, float y) { this->x = x; this->y = y; }
	float getX() { return this->x; }
	float getY() { return this->y; }
};

//形参为Point类对象，传值调用，进行实参对象与形参对象结合时调用拷贝构造函数
void display(Point p)
{
	cout << p.getX() << " " << p.getY() << endl;
}

Point f()
{
	//p是一个局部对象，在当前函数执行结束时空间自动消亡
	Point p(2.3f, 3.4f);
	//在p对象消亡前执行拷贝构造函数，诞生一个全新的对象返回给函数的主调方
	return p;
}

void main()
{
	Point p1(1.2f, 2.4f);//让系统调用构造函数诞生对象p1
	//1.拿一个现有对象去初始化新诞生对象的时候，要执行拷贝构造函数
	Point p2(p1);
	//2.将一个现有对象赋值给新对象
	Point p3 = p2;
	//3.一个函数的形参为类对象(传值调用的方式)，进行实际参数与形式参数结合时
	display(p3);//实际参数是一个对象，与形参结合时拷贝调用构造函数
	//4.一个函数返回值类型为对象时，会执行拷贝构造函数
	Point p4 = f();//此行代码只会执行1次拷贝构造函数
}
