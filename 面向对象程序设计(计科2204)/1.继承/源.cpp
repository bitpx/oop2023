#include<iostream>
using namespace std;

//父类型，包含人的共同属性和行为--一般类
class Person {
private:
	string ID;
	string name;
	int age;
protected:
	double money;
public:
	void setID(string ID)
	{
		this->ID = ID;
	}
	//省略其他方法
};
//学生是一种人
class Student :public Person {//学生类继承于父类型--特殊类
private:
	string stuNo;//学生类特有的属性-学号
public:
	void studying()//学生类特有的行为-学习
	{
		//...
	}
};
//老师也是一种人
class Teacher :public Person {//老师类继承于父类型--特殊类
private:
	string teacherNo;//教师类特有的属性-教工号
public:
	void teaching()//教师类特有的行为-教课
	{
		//...
	}
};

void main()
{

}