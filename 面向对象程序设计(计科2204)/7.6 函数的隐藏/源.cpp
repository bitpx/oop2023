#include <iostream>
using namespace  std;

class Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Parent(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	virtual void show()//被覆盖的版本
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
	virtual void show(int x)//被隐藏的版本
	{
		cout << pri << " " << pro << " " << pub << endl;
		cout << x << endl;
	}
	void show(int x, int y)//被隐藏的版本
	{
		cout << pri << " " << pro << " " << pub << endl;
		cout << x << "" << x << endl;
	}
};
class Son :public Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Son(int pri, int pro, int pub)
		:Parent(pri + 2, pro + 2, pub + 2) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	//1.覆盖了父类型中的相同函数(虚拟函数版本，同名同参同返回值类型)
	//2.隐藏了其他的同名函数
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};

int main() {
	//使用子类型的对象
	Son son(1, 2, 3);
	son.show();
	son.show(1);
	son.show(1, 2);

	return 0;
}
