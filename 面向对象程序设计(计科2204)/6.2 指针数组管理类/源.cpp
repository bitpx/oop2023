#include<iostream>
using namespace std;

class MyArray {
private:
	int** p;
	int rowCount;
	int columnCount;
public:
	//const常数据成员，对象数据成员
	MyArray(int r, int c) :rowCount(r), columnCount(c) {
		//让p指向一维指针数组
		p = new int* [rowCount];
		int v = 1;
		//让一维指针数组中每颗指针指向一个一维数组
		for (int i = 0; i < rowCount; i++)
		{
			p[i] = new int[columnCount];
		}
		for (int i = 0; i < rowCount; i++)
		{
			for (int j = 0; j < columnCount; j++)
			{
				//p[i][j] = v++;
				*(*(p + i) + j) = v++;
			}
		}
	}
	~MyArray() {
		for (int i = 0; i < rowCount; i++)
		{
			delete[] p[i];
		}
		delete[] p;
	}
	int getRowCount() const
	{
		return rowCount;
	}
	int getColumnCount() const
	{
		return columnCount;
	}
	int getData(int i, int j) const
	{
		return p[i][j];
	}
};

void main()
{
	MyArray* o = new MyArray(3, 4);
	for (int i = 0; i < o->getRowCount(); i++)
	{
		for (int j = 0; j < o->getColumnCount(); j++)
		{
			cout << o->getData(i, j) << " ";
		}
		cout << endl;
	}
	delete o;
}