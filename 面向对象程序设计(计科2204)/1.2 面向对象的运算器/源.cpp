#include<iostream>
using namespace std;

//类类型--数据类型
class Calculator {
private://私有权限段
	int a, b;
public://公有段
	void setValue(int x, int y)
	{
		a = x;
		b = y;
	}
	int getSum()
	{
		return a + b;
	}
	int getSub()
	{
		return a - b;
	}
};

class Student {
private://数据成员一般放置于私有段保护起来
	string no, name;
	short age;
public:
	void setAge(short age)
	{
		if (age < 8 || age>60)
		{
			return;
		}
		this->age = age;
	}
	void setValue(string _no, string name, short age)
	{
		no = _no;
		this->name = name;
		this->age = age;
	}
	void studying()
	{
		cout << name << "正在学习!" << endl;
	}
	void eating()
	{
		cout << name << "正在吃饭!" << endl;
	}

};

void main()
{
	//将学生类Student实例化为一个对象
	Student s1;
	//s1.age = 10;
	s1.setAge(1000);


	//声明了复杂的变量c(将Calculator类实例化为对象c)，
	//更应称为对象
	Calculator c;
	int sum, sub;

	c.setValue(1, 2);
	sum = c.getSum();
	sub = c.getSub();

	cout << sum << " " << sub << endl;

	Calculator d;//又将类Calculator实例化为对象d
	/*
	 d.a = 4;
	 d.b = 5;
	*/
	d.setValue(4, 5);
	sum = d.getSum();
	sub = d.getSub();

	cout << sum << " " << sub << endl;

}