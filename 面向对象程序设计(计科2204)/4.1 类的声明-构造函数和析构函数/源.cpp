#include<iostream>
using namespace std;

class Point
{
private:
	float x, y;
public:
	Point(float x, float y) {
		this->x = x; this->y = y;
	}
	Point() {
		//this->x = 0; this->y = 0;
		Point(0, 0);
	}
	void setValue(float x, float y) { this->x = x; this->y = y; }
	float getX() { return this->x; }
	float getY() { return this->y; }
};

class Tools {
public:
	// 静态函数，属于类，不属于对象
	static void checkValidAndAssign(int& varible, int value, int maxValue = 59, int minValue = 0)
	{
		if (value > maxValue || value < minValue)
		{
			varible = 0;
		}
		else
		{
			varible = value;
		}
	}
};


class Clock {
private:
	int hour, minute, second;
protected:
public:
	// 构造函数，在对象诞生的时候给对象初始化
	Clock(int hour, int minute, int second)
		:hour(hour), /*minute(minute),*/ second(second) // 初始化列表
	{
		/*this->hour = hour;
		this->second = second;*/
		this->minute = minute;
	}
	void setHour(int hour)
	{
		// 调用类Tools中的函数checkValidAndAssign来检查数据并赋值
		Tools::checkValidAndAssign(this->hour, hour, 23);

		/*if (hour >= 24 || hour < 0)
		{
			this->hour = 0;
		}
		else
		{
			this->hour = hour;
		}*/
	}
	void setMinute(int minute)
	{
		Tools::checkValidAndAssign(this->minute, minute);
		/*if (minute >= 60 || minute < 0)
		{
			this->minute = 0;
		}
		else
		{
			this->minute = minute;
		}*/
	}
	void setSecond(int second)
	{
		Tools::checkValidAndAssign(this->second, second);
		/*if (second >= 60 || second < 0)
		{
			this->second = 0;
		}
		else
		{
			this->second = second;
		}*/
	}
	int getHour() { return this->hour; }
	int getMinute() { return minute; }
	int getSecond() { return second; }

	/*void display() {
		cout << hour << ":"
			<< minute << ":"
			<< second << endl;
	}*/
};

void main()
{
	// 先诞生对象，后赋值
	Point p1;
	p1.setValue(1.0f, 2.0f);
	// 在诞生对象的同时给数据常用初始化
	Point p2(2.0f, 1.2f);

	Point* p3 = new Point();// 空括号可省略
	Point* p4 = new Point(3.0f, 4.0f);// 空括号可省略



	// 将Clock类实例化为对象
	// c1是一个真正对象，存储于栈中，类的实例化
	Clock c1(11, 12, 13);// 在对象诞生的时候初始化数据成员
	// 先诞生对象，后赋值
	c1.setHour(12);
	c1.setMinute(25);
	c1.setSecond(45);
	cout << c1.getHour() << ":"
		<< c1.getMinute() << ":"
		<< c1.getSecond() << endl;
	// c2不是一个真正对象，而是一个指针变量(栈中)，无实例化
	Clock* c2;
	//c2 = (Clock*)malloc(sizeof(Clock));
	c2 = new Clock(12, 23, 34);// 使用new来申请空间，存储于堆中，实例化
	c2->setHour(12);
	c2->setMinute(25);
	c2->setSecond(45);
	cout << c2->getHour() << ":"
		<< c2->getMinute() << ":"
		<< c2->getSecond() << endl;

	//Clock clocks[100000];// 对象数组，诞生了100000个对象，栈
	Clock* clocks[100000]; // 是指针数组，没有任何对象
}