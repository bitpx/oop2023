#include<iostream>
using namespace std;

class Student
{
private:
	string no;
public:
	Student(string no)
	{
		this->no = no;
	}
	void setNo(string no)
	{
		this->no = no;
	}
	string getNo()
	{
		return this->no;
	}
	//常函数
	string getNo() const
	{
		return this->no;
	}
};

void main()
{
	//普通对象
	Student s1("1001");
	//如果有普通函数和常函数，默认调用普通函数
	//如果只有常函数，就调用常函数
	cout << s1.getNo() << endl;

	//常对象，只能调用常函数
	const Student s2("1002");
	cout << s2.getNo() << endl;

	//常引用必须在声明时初始化(作为形参时例外)
	const Student& s3 = s2;
	//常引用只能调用常函数版本
	cout << s3.getNo() << endl;
}





