#include<iostream>
using namespace std;

class Hour
{
private:
	int data;
public:
	~Hour() {
		cout << "局部类Hour对象消亡了" << endl;
	}

	Hour(int data) {
		this->data = data;
		cout << "局部类Hour对象诞生了" << endl;
	}
	//其他函数省略
};

class Minute
{
private:
	int data;
public:
	~Minute() {
		cout << "局部类Minute对象消亡了" << endl;
	}

	Minute(int data) {
		this->data = data;
		cout << "局部类Minute对象诞生了" << endl;
	}
	//其他函数省略
};

class Second
{
private:
	int data;
public:
	~Second() {
		cout << "局部类Second对象消亡了" << endl;
	}

	Second(int data) {
		this->data = data;
		cout << "局部类Second对象诞生了" << endl;
	}
	//其他函数省略
};


class Clock {//整体类-整车
private:
	int h, m, s;//常规的基础类型作为数据成员
	Hour hour;//局部类对象作为数据成员
	Minute minute;//局部类对象作为数据成员
	Second second;//局部类对象作为数据成员
public:
	// 构造函数，在对象诞生的时候给对象初始化
	Clock(int h, int m, int s)
		:second(s), hour(h), minute(m), h(h), s(s)
	{
		this->m = m;
		cout << "整体类Clock对象诞生了" << endl;
	}
	~Clock() {
		cout << "整体类Clock对象消亡了" << endl;
	}
};

void main()
{
	Clock* c = new Clock(12, 13, 14);//将对象存储于堆中
	// 使用对象c......
	delete c;
}