#include<iostream>
using namespace std;
class GrandFather {
private:
	int data;
public:
	GrandFather(int data) :data(data)
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
	~GrandFather()
	{
		cout << "GrandFather对象消亡了！" << endl;
	}
	void show(int x, int y) { cout << data << endl; }
};

class Father :virtual public GrandFather {
public:
	Father(int data) :GrandFather(data)
	{
		cout << "Father对象诞生了！" << endl;
	}
	~Father()
	{
		cout << "Father对象消亡了！" << endl;
	}
};

class Mother :virtual public GrandFather {
public:
	Mother(int data) :GrandFather(data)
	{
		cout << "Mother对象诞生了！" << endl;
	}
	~Mother()
	{
		cout << "Mother对象消亡了！" << endl;
	}
};

class Son :public Father, public Mother {
public:
	Son(int data)
		:Father(data),
		Mother(data),
		GrandFather(data)
	{
		cout << "Son对象诞生了！" << endl;
	}
	~Son()
	{
		cout << "Son对象消亡了！" << endl;
	}
};


void main()
{
	Son* o1 = new Son(1);
	//使用
	o1->show(1, 2);
	delete o1;
	{
		Son son(2);
		son.show(1, 2);
	}
}