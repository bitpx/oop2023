#include<iostream>
#define N 5
using namespace std;

// 传值调用
/*
   结果：对形参的操作不会影响到实际参数
*/
void addOne(int x)
{
	x++;
}

// 传地址调用
/*
   1.形参是指针变量，是数组(本质上也是指针)
   结果：对形参的操作直接影响到实际参数
*/
void addOne(int* x, int array[])
{
	(*x)++;
	for (int i = 0; i < N; i++)
	{
		array[i]++;
	}
}
// 引用调用
/*
*   形参是实际参数的1个别名，形参称为对实际参数一个引用
*   形参称为引用变量，实现对实参的引用
*   代码特点：在传值调用基础上，仅需在形参前加“&”
*/
void anotherAddOne(int& x)
{
	x++;
}


void main()
{
	int a = 1, ints[N] = { 1,2,3,4,5 };
	addOne(a);
	addOne(&a, ints);
	anotherAddOne(a);
}