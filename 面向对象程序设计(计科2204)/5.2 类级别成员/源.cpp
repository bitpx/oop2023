/*
  统计学生对象的人数
*/
#include<iostream>
using namespace std;

/*
   对象包含了静态的特征和动态的行为
   学生:
	 静态特征：no、name、gender、....
	 动态特征(动态的行为):吃饭、学习、.....
*/

class Student
{
private:
	//实例(对象)级别的成员，每个对象都有一个副本
	string no;
	//静态(static单词的翻译)成员，不属于对象
	//属于类，整个类中只有一个副本，为当前类所有独享共享
	//类级别的成员
	static int n;//存储静态数据区(全局数据区)-静态生存期
public:
	//类级别的函数成员，整个类中只有一个副本
	//属于类，为当前类的所有对象共享
	//类级别函数不能使用this指针
	//约定：类级别的成员函数一般只访问类级别的数据成员
	static int getN(/*const Student& s*/)
	{
		//cout << s.no << endl;
		//s.setNo("1002");
		//cout << this->n << endl;
		return Student::n;
	}
	//实例(对象)级别的成员
	Student(string no)
	{
		this->no = no;
		n++;
	}
	void setNo(string no) //实例(对象)级别的成员
	{
		//Student::n++;
		//this->n++;虽然语法正确，但不推荐使用，因为会引起歧义
		this->no = no;
	}
	//实例(对象)级别的成员
	string getNo() { return this->no; }
};
//函数的原型声明
int Student::n = 0;
void main()
{
	Student s1("1001");
	Student s2("1002");
	cout << "学生对象的数量为：" << Student::getN() << endl;
	//当前是s1对象调用getN函数
	cout << "学生对象的数量为：" << s1.getN() << endl;
	//当前是s2对象调用getN函数
	cout << "学生对象的数量为：" << s2.getN() << endl;

	/*
	   弊端：
	   1.出现相同的代码重复编写现象，有50个对象，n++要写50行
	   2.统计学生对象的变量n与学生毫无关联，不符合面向对象的编码模式
	*/
	//存储学生数量
	/*int n = 0;
	Student s1("1001");
	n++;
	Student s2("1002");
	n++;
	cout << "学生对象的数量为：" << n << endl;*/
}





