#include<iostream>
using namespace std;

void main()
{
	//动态申请变量空间
	int a = 5;//栈中
	int* p1 = new int;//在堆中申请一个空间
	int* p2 = new int(5);//在堆中申请一个空间，初始化值为5
	delete p2;
	//动态申请数组空间
	int b[3] = { 1,2,3 };//栈中
	int* p3 = new int[3] { 1, 2, 3 };//堆中
	delete[] p3;

	//二维数组的空间申请
	int c[2][3] = { {1,2,3}, {4,5,6} };//栈中
	int(*p4)[3] = new int[2][3]{ {1,2,3}, {4,5,6} };//堆中
	delete[] p4;
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << *(*(p4 + i) + j) << " ";
		}
		cout << endl;
	}

	//三维数组的空间申请
	int d[2][3][4];//栈中
	int(*p5)[3][4] = new int[2][3][4];//堆中
	delete[] p5;

	//四维数组的空间申请
	int e[2][3][4][5];//栈中
	int(*p6)[3][4][5] = new int[2][3][4][5];//堆中

	//指针数组
	int(*p7)[3];//一个指向二维数组的指针变量
	int* ints[3];//栈中，一维数组，每个单元存储一个指针，指针数组
	int** p = ints;//二维指针变量指向指针数组
	p = new int* [3];

}