#include<iostream>
using namespace std;

class Student
{
private:
	string no;
	//声明友元函数(此函数就是Student类“闺蜜”)
	friend void printNo(const Student& s);
	//声明友元类，友元类中所有的函数可直接访问当前类的私有段成员
	friend class PrintForNo;
public:
	Student(string no)
	{
		this->no = no;
	}
	/*Student()
	{
	}*/
	void setNo(string no)
	{
		this->no = no;
	}
	string getNo() const
	{
		//this->no = "1002";
		return this->no;
	}
};

void printNo(const Student& s)
{
	//友元函数可以直接访问对象的私有段
	cout << s.no << endl;
	//有性能的损失
	//cout << s.getNo() << endl;
}
class PrintForNo {//整体类
private:
	Student s;//局部类对象作为数据常用
public:
	PrintForNo(string no) :s(no)
	{}
	void print()
	{
		cout << s.no << endl;
	}
	void display()
	{
		cout << s.no << endl;
	}
};


void main()
{

}





