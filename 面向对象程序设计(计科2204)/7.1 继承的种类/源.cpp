#include<iostream>
using namespace std;
class GrandFather {};
//GrandFather为父类型，Father为子类型
//子类型Father继承于父类型GrandFather
//父类型GrandFather派生出子类型Father
//单继承、私有继承
class Father :private GrandFather {
};
//GrandFather为父类型，Mother为子类型
//子类型Mother继承于父类型GrandFather
//父类型GrandFather派生出子类型Mother
//单继承、保护继承
class Mother :protected GrandFather {
};
//多继承，公有继承
class Son :public Father, public Mother {
};

class Base {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	void show() { cout << pri << pro << pub << endl; }
};
class Derived1 :private Base {
	//private:
	//	int pro;
	//	int pub;
	//	void show() { cout << pri << pro << pub << endl; }
public:
	void f1()
	{
		cout << /*pri <<*/ pro << pub << endl;
		show();
	}
};
class Derived2 :protected Base {
	//protected:
	//	int pro;
	//	int pub;
	//	void show() { cout << pri << pro << pub << endl; }
public:
	void f2()
	{
		cout << /*pri <<*/ pro << pub << endl;
		show();
	}
};

class Derived3 :public Base {
	//protected:
	//	int pro;
	//public:
	//	int pub;
	//	void show() { cout << pri << pro << pub << endl; }
public:
	void f3()
	{
		cout << /*pri <<*/ pro << pub << endl;
		show();
	}
};


void main()
{

	Derived1 o1;
	o1.f1();
	Derived2 o2;
	o2.f2();
	Derived3 o3;
	//cout << o3.pro << endl;
	cout << o3.pub << endl;
	o3.show();
	o3.f3();
}