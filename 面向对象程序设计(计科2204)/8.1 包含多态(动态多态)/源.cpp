#include<iostream>
using namespace std;

//图形系统：求图形的面积和周长
//定义一个共同的父类型，包含各个图形共有的属性和行为
//包含纯虚函数的类称为抽象类，不能被实例化为对象
class Shape
{
private:
	string name;
public:
	Shape(string name) :name(name) {}
	void setName(string name) { this->name = name; }
	string getName() { return name; }
	//规定凡是图形都必须求面积
	//纯虚函数，又称抽象函数
	virtual double getArea() = 0;
	//规定凡是图形都必须求周长
	//纯虚函数，又称抽象函数
	virtual double getPerimeter() = 0;
};

class Rectangle :public Shape
{
private:
	double length;
	double width;
public:
	Rectangle(string name, double length, double width)
		:length(length), width(width), Shape(name)
	{	}
	//覆盖(实现)父类型规定的功能
	double getArea() {
		return length * width;
	}
	//覆盖(实现)父类型规定的功能
	double getPerimeter() {
		return 2 * (length + width);
	}
};
class Square :public Shape
{
private:
	double width;
public:
	Square(string name, double width)
		:width(width), Shape(name)
	{}
	//覆盖(实现)父类型规定的功能
	double getArea() {
		return width * width;
	}
	//覆盖(实现)父类型规定的功能
	double getPerimeter() {
		return 4 * width;
	}
};
void main()
{

	Shape shape("图形");
	Shape* s = new Shape("图形");


	//求长方形的面积和周长
	//父类型的指针指向子类型对象
	Shape* p = new Rectangle("长方形", 5.0, 4.0);
	double area = p->getArea();
	double peri = p->getPerimeter();
	cout << area << endl;
	cout << peri << endl;

	//求正方形的面积和周长
	//父类型的指针指向子类型对象
	p = new Square("正方形", 5.0);
	area = p->getArea();
	peri = p->getPerimeter();
	cout << area << endl;
	cout << peri << endl;

	////求长方形的面积和周长
	//Rectangle* p = new Rectangle("长方形", 5.0, 4.0);
	//double area = p->getArea();
	//double peri = p->getPerimeter();
	//cout << area << endl;
	//cout << peri << endl;

	////求正方形的面积和周长
	//Square* q = new Square("正方形", 5.0);
	//area = q->getArea();
	//peri = q->getPerimeter();
	//cout << area << endl;
	//cout << peri << endl;
}