#include<iostream>
using namespace std;
//业务逻辑
int getSum(int x, int y)
{
	return x + y;
}
//业务逻辑
int getSub(int x, int y)
{
	return x - y;
}

int main()
{
	int a, b;//被操作的两个数据
	int sum, sub;//保存和与差

	//界面逻辑-控制台界面
	cin >> a >> b;//输入两个整数

	//进行具体的运算
	//业务逻辑代码--通用性很强
	/*sum = a + b;
	sub = a - b;*/
	sum = getSum(a, b);
	sub = getSub(a, b);

	//界面逻辑
	cout << sum << "  " << sub << endl;

	return 0;
}