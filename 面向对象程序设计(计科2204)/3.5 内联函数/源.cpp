#include<iostream>
using namespace std;
// 内联函数：在编译阶段，
//          编译器将函数调用语句
//          直接替换成最终需要执行的代码片段
// 优势：提高运行性能
/*
   成为内联函数的条件：
   1.不存在递归调用
   2.函数体简单，业务不复杂
*/
inline int add(int x, int y) {
	add(x, y);
	return x + y;
}

void main()
{
	int a = 1, b = 2, sum1, sum2;

	//sum = a + b;//业务逻辑
	sum1 = a + b;

	sum2 = 3 + 4;

	cout << sum1 << "   " << sum2;//界面逻辑
}