#include<iostream>
using namespace std;

/*求几个整数和*/
//求两个整数的和
int getSumTwoInts(int a, int b)
{
	return a + b;
}
//求三个整数的和
int getSumThreeInts(int a, int b, int c)
{
	return a + b + c;
}
//求四个整数的和
int getSumFourInts(int a, int b, int c, int d)
{
	return a + b + c + d;
}

/*
  函数重载(充分必要的条件)：
  1.多个函数取相同的函数名
  2.形参的个数或类型必须不一致
  3.形参类型和个数，更换形参顺序实现函数重载(毫无实际意义，不推荐)
  无关：
  1.与形参的名称无关
  2.与函数的返回值类型无关
*/

//求两个整数的和
int getSum(int a, int b)
{
	return a + b;
}
//求三个整数的和
int getSum(int a, int b, int c)
{
	return a + b + c;
}
//求四个整数的和
int getSum(int a, int b, int c, int d)
{
	return a + b + c + d;
}

// 比较整数和实数，返回较大的一个数
float getMax(int a, float b)
{
	return a > b ? a : b;
}
float getMax(float b, int a)
{
	return a > b ? a : b;
}


void main()
{
	int x = 1, y = 2, z = 3, e = 4;
	int sum;
	sum = getSum(x, y);
	sum = getSum(x, y, z);
	sum = getSum(x, y, z, e);
}