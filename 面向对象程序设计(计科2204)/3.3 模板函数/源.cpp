#include<iostream>
using namespace std;

// 比较两个数的大小，返回较大的一个数
//int getMax(int a, int b)
//{
//	return a > b ? a : b;
//}
//float getMax(float a, float b)
//{
//	return a > b ? a : b;
//}
//double getMax(double a, double b)
//{
//	return a > b ? a : b;
//}
//char getMax(char a, char b)
//{
//	return a > b ? a : b;
//}

// 函数模板
/*
*  T：类型变元--是所有数据类型的代表
*/
template <class T>
T getMax(T a, T b)
{
	return a > b ? a : b;
}

void main()
{
	int r1;	float r2;	double r3;	char r4;

	r1 = getMax(1, 2);
	r2 = getMax(1.1f, 2.2f);
	r3 = getMax(1.1, 2.2);
	r4 = getMax('a', 'b');
}