#include<iostream>
using namespace std;

class Complex
{
private:
	double x, y;
	friend Complex& operator+(const Complex& a, const  Complex& b);
	friend Complex& operator-(const Complex& a, const  Complex& b);
public:
	Complex(double x, double y) :x(x), y(y) {}
	double getX() const { return x; }
	double getY()const { return y; }
};

//+重载函数，非成员函数
Complex& operator+(const Complex& a, const  Complex& b)
{
	Complex c(a.x + b.x, a.y + b.y);
	return c;
}
//+重载函数，非成员函数
Complex& operator-(const Complex& a, const  Complex& b)
{
	Complex c(a.x - b.x, a.y - b.y);
	return c;
}

void main()
{
	Complex a(1, 2), b(3, 4);
	Complex c = a + b;//operator+(a, b)
	cout << "(" << c.getX() << "," << c.getY() << ")" << endl;
	cout << c;
	Complex d = a - b;//operator-(a, b)
	cout << "(" << d.getX() << "," << d.getY() << ")" << endl;
}