#include<iostream>
using namespace std;

//存储于静态数据区
int i; //声明了一个全局变量，默认的最大的命名空间（系统在编译阶段自动添加）的全局变量
void f1()
{
	i = 1;
}
void f2()
{
	//存储栈区
	int i = 1;//与前方的全局变量i同名，覆盖全局变量i在当前函数的作用域
	cout << i << endl;//i为当前函数的局部变量i
}
namespace NS {
	namespace NS2 {
		//.....
	}
	int j = 1;//存储于静态数据区
	void f3()
	{
		//cout << x;
		cout << j;
	}
	class Point {
	public:
		Point(float x, float y)
		{
			this->x = x;
			this->y = y;
		}
	private:
		float x;
		float y;
	};
}

void f4()
{
	int x = 0;
	switch (x)
	{
	case 0:
	{
		int i = 0;
		break;
	}
	case 1:
	{
		int i = 1;
		break;
	}
	default:
		break;
	}
	int i = 1;
	//使用i

	int i = 1;
	//使用i
}

void main()
{
	i = 5;//全局变量i
	int x = 1, y = 5, sum;//内部变量
	int add(int x = 0, int y = 0);//函数的原型声明
	NS::j = 6;//使用命名空间全局变量j
	//代码块
	{
		using namespace NS;
		int i;//定义了一个局部变量(块级别)，覆盖了前面的全局变量i
		i = 7;
		cout << "i=" << i << endl;
		cout << "j=" << j << endl;
	}//代码块中的局部变量i到此结束
	sum = add(x, y);
	cout << "i=" << i << endl;//全局变量i
}


//x、y是两个形参，是两个局部变量，作用域仅限函数头和整个函数体
int add(int x = 0, int y = 0)
{
	//内部变量，作用域仅限当前行至函数体结束
	int z = 5; cout << z << endl;
	//int x = 12;//同级别范围中，不能声明同名的变量
	x++;
	for (int i = 0; i < 15; i++)//i为块级别变量，作用域仅限当前代码块
	{
		cout << i << endl;
	}

	//以下的for循环与上面的for循环属于不同的代码块，可使用相同的变量名
	for (int i = 0; i < 15; i++)//i为块级别变量，作用域仅限当前代码块
	{
		cout << i << endl;
	}


	return x + y + z;
}