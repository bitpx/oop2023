#include<iostream>
using namespace std;

class A {
public:
	void f();
};
class B {};

void A::f()
{
	B o;
}


class Base
{
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Base(int pri, int pro, int pub);
	Base(const Base& obj);
	void show();
	virtual void print();
};


inline void Base::show()
{
	cout << pri << " " << pro << " " << pub << endl;
}

void Base::print() {}
Base::Base(int pri, int pro, int pub) :pri(pri)
{
	this->pro = pro;
	this->pub = pub;
}
Base::Base(const Base& obj) :pri(obj.pri)
{
	this->pro = obj.pro;
	this->pub = obj.pub;
}

class Derived :public Base
{
public:
	Derived(int x, int y, int z)
		:Base(x, y, z)
	{}
	void print()
	{
		cout << "------------" << endl;
		show();
		cout << "------------" << endl;
	}
};

void main()
{
	Base b1(1, 2, 3);
	b1.show();

	//1.使用类型对象去初始化父类型对象
	Derived son(1, 2, 3);
	/*son.show();
	son.print();*/
	Base b2 = son;
	b2.show();

	//2.使用父类型的指针指向子类型的对象
	Base* b3 = new Derived(1, 2, 3);
	b3->show();
	b3->print();

	//3.使用父类型的引用指向子类型的对象
	/*Base& b4 = son;
	b4.show();
	b4.print();*/
}