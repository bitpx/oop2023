#include<iostream>
using namespace std;

class ArrayData {
private:
	double** p;
	int rowCount;
	int columnCount;
public:
	ArrayData(int r, int c) :rowCount(r), columnCount(c)
	{
		//让指针变量p指向一个指针数组
		p = new double* [rowCount];
		//让指针数组的每个单元存储的指针指向具体的一维数组
		for (int i = 0; i < rowCount; i++)
		{
			p[i] = new double[columnCount];
		}
		for (int i = 0; i < rowCount; i++)
		{
			for (int j = 0; j < columnCount; j++)
			{
				p[i][j] = (i + 1) * (j + 1);
			}
		}
	}
	~ArrayData() {
		//释放每颗指针指向的一维数组空间
		for (int i = 0; i < rowCount; i++)
		{
			delete[] p[i];
		}
		delete[] p;
	}
	double getData(int i, int j) const
	{
		return p[i][j];
	}
	int getRowCount() const
	{
		return this->rowCount;
	}
	int getColumnCount() const
	{
		return columnCount;
	}
};

void main()
{
	ArrayData array(4, 4);
	int i, j;
	for (i = 0; i < array.getRowCount(); i++)
	{
		for (j = 0; j < array.getColumnCount(); j++)
		{
			cout << array.getData(i, j) << " ";
		}
		cout << endl;
	}
}