#include<iostream>
using namespace std;

class Parent {
private:
	int data;
public:
	Parent(int data) :data(data)
	{

	}
	//虚拟函数
	virtual void show() { cout << data << endl; }
};

class Son :public  Parent {
private:
	int data;
public:
	Son(int data) :data(data), Parent(data + 2)
	{
	}
	//既能输出父类型中的data,也能输出子类型中的data
	virtual void show()
	{
		//输出父类型中声明的data
		Parent::show();
		//输出子类型的data
		cout << this->data << endl;
	}
};


void main()
{
	Parent p1(1);
	p1.show();


	Parent* p = new Son(1);
	p->show();


	Son son(2);
	Parent& q = son;



	q.show();
}