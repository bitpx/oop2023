#include "Rectangle.h"
#include<iostream>
using namespace std;
using namespace My;
//构造函数的实现
Rectangle::Rectangle(float height, float width)
{
	this->height = height;
	this->width = width;
}

float Rectangle::getArea()
{
	return this->height * this->width;
}

float Rectangle::getPerimeter()
{
	return (this->height + this->width) * 2;
}

