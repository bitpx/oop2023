#pragma once //不管当前文件被include多少次，都只会被编译1次
//只负责功能的声明，不负责具体的实现
namespace My {
	//长方形类
	class Rectangle {
	private:
		float height;//长
		float width;//宽
	public:
		Rectangle(float height, float width);
		float getArea();//求面积
		float getPerimeter();//求周长
	};
}
