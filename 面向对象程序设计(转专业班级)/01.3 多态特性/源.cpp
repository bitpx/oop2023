#include<iostream>
using namespace std;
class Shape {//规定只要是图形，就必须求面积和周长
public:
	virtual double GetArea() = 0;//纯虚函数，抽象函数
	virtual double GetPerim() = 0;//纯虚函数，抽象函数
	virtual void SetValue(double l, double w)//虚函数
	{

	}
	virtual void SetValue(double w)
	{

	}
};

class Rectangle :public Shape {
private:
	double length;
	double width;
public:
	void SetValue(double l, double w) {
		length = l;
		width = w;
	}
	double GetArea()
	{
		return length * width;
	}
	double GetPerim() {
		return (length + width) * 2;
	}
};

class Square :public Shape {
private:
	double width;
public:
	void SetValue(double w) {
		width = w;
	}
	double GetArea()
	{
		return width * width;
	}
	double GetPerim() {
		return width * 4;
	}
};

void main()
{
	/*Rectangle r;
	r.SetValue(1, 2);
	cout << r.GetArea() << endl;
	cout << r.GetPerim() << endl;

	Square s;
	s.SetValue(1);
	cout << s.GetArea() << endl;
	cout << s.GetPerim() << endl;*/

	Shape* s;
	s = new Rectangle();
	s->SetValue(1, 2);
	cout << s->GetArea() << endl;
	cout << s->GetPerim() << endl;
	s = new Square();
	s->SetValue(1);
	cout << s->GetArea() << endl;
	cout << s->GetPerim() << endl;


}