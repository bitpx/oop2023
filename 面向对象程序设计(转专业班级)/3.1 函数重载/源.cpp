#include<iostream>
using namespace std;

/*
   实现两个整数、三个整数、四个整数的加法运算
*/
int getSum(int a, int b)
{
	return a + b;
}
int getSum(int a, int b, int c)
{
	return a + b + c;
}
int getSum(int a, int b, int c, int d)
{
	return a + b + c + d;
}

// 从语法上，如下的两个函数也能重载成功，但是毫无意义
float getMax(int x, float y)
{
	return x > y ? x : y;
}
float getMax(float y, int x)
{
	return x > y ? x : y;
}

void main()
{
	int a = 1, b = 2, c = 4, d = 4;
	int sum;

	sum = getSum(a, b);
	sum = getSum(a, b, c);
	sum = getSum(a, b, c, d);
}