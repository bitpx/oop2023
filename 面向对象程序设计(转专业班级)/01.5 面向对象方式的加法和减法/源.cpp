#include<iostream>
using namespace std;

class Calculator {
private:
	//被操作的两个数据-静态的特征：数据成员
	int numberA, numberB;
public:
	//下面的函数都是操作数据的方法
	void setValue(int numberA, int numberB)
	{
		this->numberA = numberA;
		this->numberB = numberB;
	}
	int getSum()
	{
		return this->numberA + numberB;
	}
	int getSub()
	{
		return this->numberA - numberB;
	}
};

void main()
{
	//对象o1占用了空间
	Calculator o1;//o1是一个真正的对象，存储于栈中
	int sum, sub;
	o1.setValue(1, 2);
	sum = o1.getSum();
	sub = o1.getSub();
	cout << sum << "  " << sub << endl;

	//o2不是一个真正的对象，仅仅是指向对象的指针变量
	Calculator* o2 = NULL;
	o2 = new Calculator();
	o2->setValue(3, 4);
	sum = o2->getSum();
	sub = o2->getSub();
	cout << sum << "  " << sub << endl;

}