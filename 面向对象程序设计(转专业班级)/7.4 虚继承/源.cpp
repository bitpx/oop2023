#include<iostream>
using namespace std;

class GrandFather
{
private:
	int data;
public:
	GrandFather(int data) :data(data) {
		cout << "GrandFather的构造函数执行了！" << endl;
	}
	~GrandFather() {
		cout << "GrandFather的析构函数执行了！" << endl;
	}
	void show()
	{
		cout << "GrandFather中：" << data << endl;
	}
};

class Father :virtual public GrandFather {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Father(int pri, int pro, int pub)
		:GrandFather(pri)
	{
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
		cout << "Father的构造函数执行了！" << endl;
	}
	~Father() {
		cout << "Father的析构函数执行了！" << endl;
	}
};

class Mother :virtual public GrandFather {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Mother(int pri, int pro, int pub)
		:GrandFather(pri)
	{
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
		cout << "Mother的构造函数执行了！" << endl;
	}
	~Mother() {
		cout << "Mother的析构函数执行了！" << endl;
	}
};

class Son :public Father, public Mother {
private:
	int x;
protected:
	int y;
public:
	int z;
	Son(int x, int y, int z)
		:Father(x, y, z), Mother(x, y, z), GrandFather(x)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		cout << "Son的构造函数执行了！" << endl;
	}
	~Son() {
		cout << "Son的析构函数执行了！" << endl;
	}
	void print()
	{
		cout << x << " " << y << " " << z << endl;
	}
};

void main()
{
	{
		Son son(1, 2, 3);
		son.show();
	}


	/*Son* s = new Son(1, 2, 3);
	s->display(1);
	s->show();
	s->print();
	delete s;*/
}