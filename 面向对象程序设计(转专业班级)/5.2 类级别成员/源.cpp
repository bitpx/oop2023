#include<iostream> 
#include<string.h> 
using namespace std;

//统计学生的人数
//static成员，即类级别成员，或成为静态成员
class Student
{
private:
	string no;//实例级别的成员：每个对象都有1个副本，属于对象的私有财产
	string name;//实例级别的成员：每个对象都有1个副本，属于对象的私有财产
	//用static关键字来声明类级别的成员，整个类中只有一个副本，存储于静态数据区
	static int n;//统计学生的人数，属于整个类，不属于对象，但是所有对象都可以共享它
public:
	Student(string no, string name)//实例级别的函数
	{
		this->no = no;
		this->name = name;
		Student::n++;
		//this->n++;虽然语法正确，但是不推荐
	}
	void setNo(string no) { this->no = no; }//实例级别的函数
	void setName(string name) { this->name = name; }//实例级别的函数
	string getNo() { return this->no; }//实例级别的函数
	string getName() { return this->name; }//实例级别的函数
	//通常情况下，声明类级别函数访问类级别数据成员
	static int getStudentCount()//类级别的函数
	{
		//return this->n;//static函数中不能使用this
		return Student::n;
	}
	//类级别的成员函数一般只访问类级别的数据成员，以下的写法不推荐
	static string getNoOfStu(Student s)
	{
		return s.no;
	}
};

//类似于函数原型声明，声明时初始化
int Student::n = 0;

void main()
{
	Student s1("101", "小a");
	Student s2("102", "小b");
	Student s3("103", "小c");
	cout << "共有学生：" << Student::getStudentCount() << "人" << endl;
	//以下的两种方式虽然语法正确，但是不推荐
	cout << "共有学生：" << s1.getStudentCount() << "人";
	cout << "共有学生：" << s2.getStudentCount() << "人";
}
//void main()
//{
//	//保存学生的人数
//	int n = 0;
//	Student s1("101", "小a");
//	n++;
//	Student s2("102", "小b");
//	n++;
//	Student s3("103", "小c");
//	n++;
//	cout << "共有学生：" << n << "人" << endl;
//
//	/*
//	 弊端：
//	 1.n目前是独立的，与Student类毫无关系
//	 2.每实例化一个学生对象，都要执行n++，出现了代码冗余现象
//	 */
//}
