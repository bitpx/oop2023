#include<iostream>
using namespace std;

class Point {
private:
	float x, y;
public:
	Point() :x(0), y(0) {
		cout << "default Constructor called." << endl;
	}
	Point(float x, float y) :x(x), y(y) {
		cout << "Constructor called." << endl;
	}
	Point(const Point &p)
	{
		this->x = p.x;
		this->y = p.y;
		cout << "Constructor called." << endl;
	}
	~Point() {
		cout << "Destructor called." << endl;
	}
	float getX() const { return x; }
	float getY() const { return y; }
	void move(float newX, float newY)
	{
		x = newX;
		y = newY;
	}
};

void main()
{
	Point* p1 = new Point;//执行无参的构造函数
	Point* p2 = new Point(1.1f, 2.2f);//执行有参的普通构造函数
	Point* p2 = new Point(*p2);//执行拷贝构造函数
}