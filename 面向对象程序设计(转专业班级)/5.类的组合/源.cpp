#include<iostream>
using namespace std;

class A {
private:
	int a;
public:
	A(int a) :a(a)
	{
		cout << "A类对象诞生了！" << endl;
	}
	~A()
	{
		cout << "A类对象消亡了！" << endl;
	}
};
class B {
private:
	int b;
public:
	B(int b) :b(b)
	{
		cout << "B类对象诞生了！" << endl;
	}
	~B()
	{
		cout << "B类对象消亡了！" << endl;
	}
};
class Box {//整体类
private:
	int x, y;//普通的数据成员，可通过构造函数或初始化列表初始化
	//成员对象必须通过初始化列表初始化
	A o1;
	B o2;
	//常数据成员必须通过初始化列表初始化
	const int z;
public:
	Box(int x, int y) :y(y), o1(x), o2(y), z(x)
	{
		this->x = x;
		//this->z = z;常数据成员不能通过构造函数体初始化
		cout << "C类对象诞生了！" << endl;
	}
	~Box()
	{
		cout << "C类对象消亡了！" << endl;
	}
};

void main()
{
	Box box(1, 2);
}