#include<iostream>
using namespace std;

class Parent
{
public:
	virtual ~Parent()
	{
		cout << "父类型对象消亡了!" << endl;
	}
};
class Son :public Parent
{
public:
	~Son()
	{
		cout << "子类型对象消亡了!" << endl;
	}
};

void main()
{
	Parent* p = new Son();

	delete p;

}