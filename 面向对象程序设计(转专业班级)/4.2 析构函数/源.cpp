#include<iostream>
using namespace std;

class Hour {
private:
	int data;
public:
	Hour(int data)
	{
		this->data = data;
		cout << "小时类对象：Hour小时对象诞生了！" << endl;
	}
	~Hour()
	{
		cout << "小时类对象：Hour小时对象消亡了！" << endl;
	}
};

class Minute {
private:
	int data;
public:
	Minute(int data)
	{
		this->data = data;
		cout << "分钟类对象：Minute分钟对象诞生了！" << endl;
	}
	~Minute()
	{
		cout << "分钟类对象：Minute分钟对象消亡了！" << endl;
	}
};

class Second {
private:
	int data;
public:
	Second(int data)
	{
		this->data = data;
		cout << "局部类对象：Second秒对象诞生了！" << endl;
	}
	~Second()
	{
		cout << "局部类对象：Second秒对象消亡了！" << endl;
	}
};


class Clock { //整体类
private:
	// 类类型的数据成员，只能通过初始化列表完成初始化
	Hour hour; //局部类,直接在栈中诞生一个对象
	Minute minute; //局部类,直接在栈中诞生一个对象
	Second second; //局部类,直接在栈中诞生一个对象
	int h, m, s; // 基础类型的数据成员，可通过构造函数体或初始化列表完成初始化
protected:
public:
	Clock(int hour, int minute, int second)
		// 只能通过初始化列表实现成员对象的初始化
		:hour(hour), minute(minute), second(second), m(minute), s(second)
	{
		h = hour;
		cout << "整体类：Clock类对象诞生了！" << endl;
	}
	~Clock()
	{
		cout << "整体类：Clock类对象消亡了！" << endl;
	}
};



void main() {

	Clock* c = new Clock(12, 13, 14);
	//使用中........
	delete c;
}