#pragma once
#include<iostream>
using namespace std;

//全局变量i，其他文件可以来共享此变量
int i = 10;

//匿名的命名空间，空间类的变量，其他文件不能直接共享
namespace {
	int a = 20;
}

void Print()
{
	cout << i << endl;
	cout << a << endl;
}