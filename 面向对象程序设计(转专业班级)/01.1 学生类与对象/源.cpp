#include<iostream>
using namespace std;

//声明学生类类型-模具
class Student {
private://私有段
	//静态的特征(属性、数据成员)
	string no;//学号
	string name;//姓名
	string gender;
public: //公有段
	//动态的特征(行为、函数成员)
	void SetValue(string no, string _name, string _gender)
	{
		this->no = no;
		name = _name;
		gender = _gender;
	}
	void Display()
	{
		cout << "学号为:" << no << "，姓名为:" << name << "，性别为:" << gender << endl;
	}
	void Studying()
	{
		cout << "学生" << no << "正在学习" << endl;
	}
	void PlayBasketball()
	{
		cout << "学生" << name << "正在打篮球" << endl;
	}

	void PlayFootball()
	{
		cout << "学生" << name << "正在踢足球" << endl;
	}

};

void main()
{
	//使用模具生产对象
	Student stu1;//将类实例化为具体的对象stu1
	Student stu2;//将类实例化为具体的对象stu2
	stu1.SetValue("100", "aaa", "male");
	stu1.Display();
	stu1.PlayBasketball();
	stu1.PlayFootball();
	stu2.SetValue("101", "bbb", "female");
	stu2.Display();
	stu2.PlayBasketball();
	stu2.PlayFootball();
}