#include<iostream>
using namespace std;

class Point {
private:
	float x, y;
	const float z;//常数据成员,必须初始化，只能通过初始化列表初始化
public:
	//初始化语句执行的时间点，就是内部数据成员空间分配时的时间点
	Point(float x, float y, float z) :z(z)
	{
		//this->z = z;
		this->x = x;
		this->y = y;
	}
	void setX(float x) { this->x = x; }
	void setY(float y) { this->y = y; }
	void modify() { x = 5.6; }
	//普通函数，主要让非常对象调用
	float getX() { return x; }
	//常函数，只能读取数据成员值，主要让常对象调用
	float getX() const
	{
		//x++;常常用函数只能读取数据，不能修改数据
		return x;
	}
	float getY() { return y; }
	float getY() const
	{
		return y;
	}
};
void main()
{
	float x, y;
	Point p1(1.2f, 2.4f, 3.5f);
	p1.setX(3.4f);
	p1.modify();
	x = p1.getX();
	const Point p2(1.2f, 2.4f, 3.5f);
	//p2.setX(3.4f);//常对象，只能读取，不能修改
	//p2.modify();//常对象，只能读取，不能修改
	x = p2.getX();

	const Point& p3 = p1;//p3就是一个常引用，必须在声明时初始化
	x = p3.getX();
	float y = p3.getY();
	//p3.setX(1.2f);常引用只能读取数据，不能修改
}