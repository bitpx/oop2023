#include<iostream>
using namespace std;

/*
   实现两个整数、三个整数、四个整数的加法运算
   任何数加上0都等于任何数
*/
//int getSum(int a, int b)
//{
//	return a + b;
//}
//int getSum(int a, int b, int c)
//{
//	return a + b + c;
//}
//int getSum(int a, int b, int c, int d)
//{
//	return a + b + c + d;
//}
// 带默认参数值的函数 
int getSum(int a, int b, int c = 0, int d = 0)
{
	return a + b + c + d;
}
int getSum(int a, float b)
{
	return a + b;
}

void main()
{
	int a = 1, b = 2, c = 4, d = 4;
	int sum;

	sum = getSum(a, b);
	sum = getSum(a, b, c);
	sum = getSum(a, b, c, d);
}