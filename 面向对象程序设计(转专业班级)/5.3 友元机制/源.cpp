#include<iostream>
using namespace std;

class Point {
private:
	float x, y;
	//声明displayPoint为当前类的友元函数
	friend void displayPoint(Point p);
	//声明displayPoint为当前类的友元类
	friend class FriendOfPoint;
public:
	Point(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
};
//友元函数可以直接访问朋友对象的私有段成员
void displayPoint(Point p)
{
	cout << "(" << p.x << "," << p.y << ")" << endl;
}
class FriendOfPoint {//整体类
private:
	Point p;//局部类对象
public:
	FriendOfPoint(float x, float y) :p(x, y)
	{
	}
	void display() {
		cout << "(" << p.x << "," << p.y << ")" << endl;
	}
	void print() {
		cout << "(" << p.x << "," << p.y << ")" << endl;
	}
};

void main()
{
	Point p(1.1f, 2.3f);
	displayPoint(p);
	FriendOfPoint fp(2.5f, 5.6f);
	fp.display();
	fp.print();
}