#include<iostream>
using namespace std;

class A {
private:
	int x;
	friend class B;
};
class B {
private:
	A a;
public:
	void print()
	{
		cout << a.x << endl;
	}
};

class C :public B {
private:
	A a;
public:
	void display()
	{
		cout << a.x << endl;//友元不能被继承
	}
};




class Birthday {
private:
	int year;
protected:
	int month, day;
	//Worker worker;
public:
	Birthday(int year, int month, int day) :day(day), month(month)
	{
		this->year = year;
	}
	Birthday()
	{
		
	}
	/*void Print()
	{
		cout << worker.name << endl;
	}*/
	friend class Worker;//声明友元类
};

class Worker {//整体类
private:
	//常规的数据成员
	string name;//实例级别的成员，每个对象都有一个副本
	string position;//实例级别的成员，每个对象都有一个副本
	double salary;//实例级别的成员，每个对象都有一个副本
	//类对象作为数据成员
	Birthday birthday;//局部类对象,只能通过初始化列表初始化，实例级别的成员，每个对象都有一个副本
	static int count;//类级别的成员，属于类Worker的，整个类中只有一个副本
	static double totalSalary;//类级别的成员，属于类Worker的，整个类中只有一个副本
public:
	void printBirthday()
	{
		cout << this->birthday.year << "-" << this->birthday.month
			<< "-" << this->birthday.day << endl;
	}
	void displayBirthday()
	{
		cout << this->birthday.year << "-" << this->birthday.month
			<< "-" << this->birthday.day << endl;
	}
	Worker(string name,
		string position,
		double salary, int year, int month, int day) :birthday(year, month, day)
	{
		//1.通过构造函数体初始化
		this->name = name;
		this->position = position;
		this->salary = salary;
		Worker::count++;//求职员对象个数
		//this->count++;
		Worker::totalSalary += salary;//求工资总数
	}
	static void printSalary()
	{
		cout << "This company has " << Worker::count << " workers." << endl;
		cout << "Total salary is " << Worker::totalSalary << endl;
		cout << "Average salary is " << Worker::totalSalary / Worker::count << endl;
	}
	
};

int Worker::count = 0;//必须的，对静态成员声明并初始化，类似函数的原型声明
double Worker::totalSalary = 0.0;//必须的，对静态成员声明并初始化，类似函数的原型声明

int main() {
	string name;
	string position;
	double salary;
	cin >> name >> position >> salary;
	Worker w1(name, position, salary);
	cin >> name >> position >> salary;
	Worker w2(name, position, salary);
	cin >> name >> position >> salary;
	Worker w3(name, position, salary);
	cin >> name >> position >> salary;
	Worker w4(name, position, salary);
	cin >> name >> position >> salary;
	Worker w5(name, position, salary);
	Worker::printSalary();
	return 0;
}