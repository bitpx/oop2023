#include<iostream>
using namespace std;
//求加法--业务逻辑--操作数据的方法
int getSum(int numberA, int numberB)
{
	return numberA + numberB;
}
//求减法--业务逻辑--操作数据的方法
int getSub(int numberA, int numberB)
{
	return numberA - numberB;
}

//main函数主要包含输入输出相关的界面逻辑代码
void main()
{
	//被操作的数据
	int numberA, numberB;
	int sum, sub;

	//界面逻辑代码
	cin >> numberA >> numberB;

	//业务逻辑代码
	sum = getSum(numberA, numberB);
	sub = getSub(numberA, numberB);

	//界面逻辑代码
	cout << sum << " " << sub << endl;
}