#include<iostream>
#include<vector>
#include<list>
using namespace std;

class Point {
private:
	float x, y;
public:
	Point() :x(0), y(0) {
	}
	Point(float x, float y) :x(x), y(y) {
	}
	float getX() const { return x; }
	float getY() const { return y; }
	void move(float newX, float newY)
	{
		x = newX;
		y = newY;
	}
};
void f1()
{
	//声明一个动态对象数组(空间不够，系统会自动扩展)
	vector<Point> points;
	Point p1;
	Point p2(1.2f, 2.4f);
	Point p3(3.2f, 3.4f);
	Point p4(4.2f, 4.4f);
	//将对象添加到数组的末尾
	points.push_back(p3);
	//将对象添加到数组的末尾
	points.push_back(p4);
	//将对象添加到数组的开头
	points.insert(points.begin(), p1);
	points.insert(points.begin() + 1, p2);
	for (int i = 0; i < points.size(); i++)
	{
		cout << points[i].getX() << " " << points[i].getY() << endl;
	}
	cout << "----------------" << endl;
	vector<Point>::const_iterator p;
	for (p = points.begin(); p != points.end(); p++)
	{
		cout << (*p).getX() << " " << (*p).getY() << endl;
	}
}
void f2()
{
	vector<Point*> points;
	points.push_back(new Point(3.2f, 3.4f));
	points.push_back(new Point(4.2f, 4.4f));
	points.insert(points.begin(), new Point);
	points.insert(points.begin() + 1, new Point(2.2f, 2.4f));

	for (int i = 0; i < points.size(); i++)
	{
		cout << points[i]->getX() << " " << points[i]->getY() << endl;
	}
	cout << "----------------" << endl;
	vector<Point*>::const_iterator p;
	for (p = points.begin(); p != points.end(); p++)
	{
		cout << (*p)->getX() << " " << (*p)->getY() << endl;
	}
}

void f3()
{
	list<Point*> points;
	points.push_back(new Point(3.2f, 3.4f));
	points.push_back(new Point(4.2f, 4.4f));
	points.insert(points.begin(), new Point);
	points.insert(++(points.begin()), new Point(1.1f, 1.2f));

	list<Point*>::const_iterator p;
	for (p = points.begin(); p != points.end(); p++)
	{
		cout << (*p)->getX() << " " << (*p)->getY() << endl;
	}
}
void main()
{
	f3();
}