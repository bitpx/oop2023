#include<iostream>
using namespace std;

class Killer {
public:
	void shot()
	{
		cout << "用机枪！" << endl;
	}
};
class MrHuang {
public:
	virtual void kick()
	{
		cout << "佛山无影脚！" << endl;
	}
};

class A //类的组合实现功能复用
{
private:
	Killer killer;
public:
	void shot()
	{
		killer.shot();//复用数据成员对象功能
		cout << "用大炮轰！" << endl;
	}
};
class B :public MrHuang
{
private:
	Killer killer;
public:
	void kick()
	{
		MrHuang::kick();
		cout << "降龙十八掌！" << endl;
		killer.shot();
	}
};

void main()
{
	A a;
	a.shot();
	B b;
	b.kick();
}