#include<iostream>
using namespace std;

class GrandFather
{

};
//私有的继承、单继承
class Father :private GrandFather {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Father(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	Father() {}
};
//保护继承、单继承
class Monther :protected GrandFather {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Monther(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	Monther() {}
};
//类Son继承于Father类
//Son为子类型，Father、Monther为父类型，属于多继承
class Son :public Father, public Monther {

};

class Base {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Base(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	Base() {}
	~Base() {}
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
};

class Derived1 :public Base {
	//protected:
	//	int pro;
	//public:
	//	int pub;
	//	void show()
	//	{
	//		cout << pri << " " << pro << " " << pub << endl;
	//	}
public:
	void display()
	{
		show();
		cout << pro << endl;
		cout << pub << endl;
		//cout << pri << endl;
	}
};



void main()
{
	Derived1 d1;
	d1.display();

}