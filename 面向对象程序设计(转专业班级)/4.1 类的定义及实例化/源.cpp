#include<iostream>
using namespace std;

class Tools {
public:
	//对形参的操作需要直接影响实参
	//static关键字将一个成员由对象级别变为类级别
	//static类型的成员不属于某一个具体的对象，而属于类
	static void dataCheckAndAssign(int& variable, int value,
		int maxValue = 59,
		int minValue = 0)
	{
		if (value > maxValue || value < minValue)
		{
			variable = 0;
		}
		else
		{
			variable = value;
		}
	}
};

/*
*   时钟
	数据抽象-属性：hour,minute,second
	行为抽象-服务：set方法、get方法、其他方法
	封装：只给予外界访问成员的最小权限
	hour: >=0且<=23
	minute:>=0且<=59
	second:>=0且<=59
*/
class Clock { //封装体
private:
	int hour, minute, second;
protected:
public:
	//形参obj为实际参数对象的别名，不会另外占用对象空间
	//const关键字表示只读，保护传进来的实际参数对象
	//obj是一个常引用
	Clock(const Clock& obj)
	{
		//一旦进入构造函数体，就会诞生1个全新的对象，通过this可找到此对象
		//Clock c 或 new Clock时才会诞生新对象
		this->hour = obj.hour;//当前类的函数体可直接访问私有段成员
		this->minute = obj.minute;
		this->second = obj.second;
		/*obj.hour = 17;
		obj.setHour(20);*/
	}
	// 构造函数的重载(overload)
	Clock(int hour, int minute, int second)
		//初始化列表
		:second(second), hour(hour)
	{
		//this->hour = hour;
		this->minute = minute;
		//this->second = second;
	}
	Clock()
	{
		/*this->hour = 0;
		this->minute = 0;
		this->second = 0;*/
		//此函数调用进入另外一个构造函数体时不会诞生新对象
		//原因：系统遇到Clock c或new Clock时才会诞生新对象
		Clock(0, 0, 0);
	}
	//函数默认为内联函数
	void setHour(int hour = 0)
	{
		Tools::dataCheckAndAssign(this->hour, hour, 23);
	}
	void setMinute(int minute = 0)
	{
		Tools::dataCheckAndAssign(this->minute, minute);
	}
	void setSecond(int second = 0)
	{
		Tools::dataCheckAndAssign(this->second, second);
	}
	//由于定义时没有添加static关键字，所以函数为实例(对象)级别函数
	int getHour() {
		//系统会在编译时自动添加一个this指针
		return hour;
	}
	int getMinute() { return minute; }
	int getSecond() { return second; }
};

//传值调用，形参对象需要在函数执行时分配空间，
//将对实际参数对象进行拷贝而诞生形参对象空间
//系统会自动执行拷贝构造函数，诞生形参对象空间
void f1(Clock c)
{
	cout << c.getHour() << endl;
}

Clock f2()
{
	//局部对象，当前函数执行完毕时，此对象空间自动消亡
	Clock c1(12, 13, 14);
	//返回一个对象
	//系统先执行拷贝构造函数通过对c1的拷贝诞生新对象，然后返回新对象
	return c1;
}


void main()
{
	Clock c3(12, 13, 14);

	//1.当用类的一个对象去初始化该类的另外一个对象
	Clock c4(c3);
	Clock c5 = c4;

	//2.函数的形参是类的对象，调用函数时，进行形参和实参的结合时
	f1(c5);

	//3.一个函数返回该类的对象，执行函数完成返回调用者时
	Clock c6 = f2();//此代码行只会执行1次拷贝构造函数



	//1.将类Clock实例化为1个对象c1，c1是一个真正的对象，存储于内存的栈中
	Clock c1(12, 13, 14); //c1也是一个封装体，main函数相当于封装体的外部
	c1.setHour(24);
	c1.setMinute(12);
	c1.setSecond(99);
	cout << c1.getHour() << ":"
		<< c1.getMinute() << ":"
		<< c1.getMinute() << endl;

	//2.c2并非真正的对象，只是一个指向对象的指针变量，其只存储地址
	//  c2空间存储于栈中
	Clock* c2;
	/*c2 = (Clock*)malloc(sizeof(Clock)); *///将类Clock实例化为真正的对象
	c2 = new Clock(12, 12, 13); // 在内存的堆区分配对象空间
	c2->setHour(12);
	c2->setMinute(15);
	c2->setSecond(59);
	cout << c2->getHour() << ":"
		<< c2->getMinute() << ":"
		<< c2->getMinute() << endl;

	// 需求：请定义一个数组，可以保存多个时钟的信息(时钟的个数不定)
	// 对象数组，具有10000个存储单元，每个单元是一个真正的对象
	Clock clocks1[10000];//不合适，因为可能会浪费大量的内存空间

	Clock* clocks2[10000];//每个存储单元只存储一个地址(并非存储1个对象)
	for (int i = 0; i < 10; i++)
	{
		clocks2[i] = new Clock;
	}
}