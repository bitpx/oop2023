#include<iostream>
using namespace std;

class Person {
private:
	string gender;
	short age;
protected:
	string name;
public:
	void PlayBasketball()
	{
		cout << name << "正在打篮球" << endl;
	}
	void PlayFootball()
	{
		cout << name << "正在踢足球" << endl;
	}
};

class Student :public Person {//学生Student公有继承于类Person
private:
	string sno;
public:
	void Studying()
	{
		cout << sno << name << "正在学习" << endl;
	}
};

class Teacher :public Person {//老师Teacher公有继承于类Person
private:
	string tno;
public:
	void Teaching()
	{
		cout << tno << "正在教书" << endl;
	}
};

void main()
{
	Student stu1;
	stu1.PlayBasketball();
	Teacher tea1;
	tea1.PlayFootball();
}