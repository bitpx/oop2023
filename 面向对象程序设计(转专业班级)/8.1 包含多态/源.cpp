#include<iostream>
using namespace std;

class A
{
public:
	virtual void f1() = 0;
	virtual void f2() = 0;
	virtual void f3() = 0;
};

//包含抽象函数的类称为抽象类
//抽象类不能被实例化
class Shape {
private:
	string no;
	string name;
public:
	Shape(string no, string name) :no(no), name(name) {}
	Shape(const Shape& o)
	{
		this->no = o.no;
		this->name = o.name;
	}
	//纯虚函数，抽象函数
	virtual double getArea() = 0;
	virtual double getPerimeter() = 0;
	void setNo(string no) { this->no = no; }
	string getNo() { return this->no; }
	void setName(string name) { this->name = name; }
	string getName() { return this->name; }
};
//子类型Circle由于未覆盖从父类型继承的抽象函数
//所以子类型也仍然是一个抽象类，不能被实例化
class Circle :public Shape {
private:
	double  r;
public:
	Circle(double r, string no, string name)
		:Shape(no, name)
	{
		this->r = r;
	}
};

class Square :public Shape
{
private:
	double width;
public:
	Square(string no, string name, double width)
		:Shape(no, name)
	{
		this->width = width;
	}
	Square(const Square& o)
		:Shape(o)
	{
		this->width = o.width;
	}
	double getArea() {
		return width * width;
	}
	double getPerimeter() {
		return width * 4;
	}
};

class Rectangle :public Shape
{
private:
	double width;
	double length;
public:
	Rectangle(string no, string name, double length, double width)
		:Shape(no, name)
	{
		this->width = width;
		this->length = length;
	}
	Rectangle(const Rectangle& o)
		:Shape(o)
	{
		this->width = o.width;
		this->length = o.length;
	}
	double getArea() {
		return length * width;
	}
	double getPerimeter() {
		return (width + length) * 2;
	}
};

void main()
{
	//求长方形的面积和周长
	Shape* s = new  Rectangle("rec1", "1号长方形", 4.0, 2.0);
	double area = s->getArea();
	double perimeter = s->getPerimeter();
	cout << area << " " << perimeter << endl;

	//求正方形的面积和周长
	s = new  Square("squ1", "1号正方形", 4.0);
	area = s->getArea();
	perimeter = s->getPerimeter();
	cout << area << " " << perimeter << endl;

	//求圆形的面积
	//s = new Circle(1.2, "1", "bb");


	////求长方形的面积和周长
	//Rectangle* rec = new  Rectangle("rec1", "1号长方形", 4.0, 2.0);
	//double area = rec->getArea();
	//double perimeter = rec->getPerimeter();
	//cout << area << " " << perimeter << endl;

	////求正方形的面积和周长
	//Square* squ = new  Square("squ1", "1号正方形", 4.0);
	//area = squ->getArea();
	//perimeter = squ->getPerimeter();
	//cout << area << " " << perimeter << endl;
}