#include<iostream>
#define N 5
using namespace std;

// 形参接收的是数值(传值调用)
void addOne(int x)
{
	x++;
}
// 两个形参本质上都是指针变量，接收来自于实参的地址
void addOne(int* x, int array[])
{
	(*x)++;
	for (int i = 0; i < N; i++)
	{
		array[i]++;
	}
}
// 引用调用，形参x是实际参数的1个别名
void anotherAddOne(int& x)
{
	x++;
}

void main()
{
	int a = 1, ints[N] = { 1,2,3,4,5 };
	addOne(a); // 传递的是实际参数a存储的数值1，传值调用
	addOne(&a, ints); // 传地址调用
	anotherAddOne(a); // 引用调用

}