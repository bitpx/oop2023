#include<iostream>
using namespace std;

void main()
{
	//定义一个一维数组，并用指针指向它
	int a = 2;//a存储于栈中
	int* q1 = &a;//q1指向a
	int* p1 = new int(2);//p1存储于栈中，存储2的变量存储于堆中
	delete p1;
	//指针变量指向一维数组,指针和数组空间都存储于栈中
	int b[3] = { 1,2,3 };
	int* q2 = b;
	//p2存储于栈中，数组空间存储于堆中
	int* p2 = new int[3] { 1, 2, 3 };
	delete[] p2;

	//指向二维数组的指针变量的声明方式
	//指针和数组空间都存储于栈中
	int c[2][3] = { {1,2,3} ,{4,5,6} };
	int(*q3)[3] = c;
	//指针p3存储于栈中，数组空间存储于堆中
	int(*p3)[3] = new int[2][3]{ {1,2,3} ,{4,5,6} };
	delete[] p3;

	//指向三维数组的指针变量的声明方式
	//指针和数组空间都存储于栈中
	int d[2][3][4];
	int(*q)[3][4] = d;
	//指针p存储于栈中，数组空间存储于堆中
	int(*p)[3][4] = new int[2][3][4];

	//定义一个二维指针变量指向一个指针数组结构
	//指针变量和指针数组都是存储于栈中
	char* booksName[2];
	char chs1[3] = "aa";
	char chs2[3] = "bb";
	booksName[0] = chs1;
	booksName[1] = chs2;
	char** r1;
	r1 = booksName;

	//定义一个二维指针变量指向一个指针数组结构
	//指针变量存储于栈中，指针数组存储于堆中
	char** r2 = new char* [2];//在堆中申请具有两个单元的指针数组
	r2[0] = new char[3] {'a', 'a', '\0'};
	r2[1] = new char[3] {'b', 'b', '\0'};

	//strcpy(r2[1],"bb");

	/*for (int i = 0; i < 2; i++)
	{
		puts(r2[i]);
	}*/

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			printf("%c",r2[i][j]);
		}
		putchar('\n');
	}



	/*int* p1 = new int(10);
	int* p2 = new int[4] {1, 2, 3, 4};
	int(*p3)[3] = new int[2][3];
	int(*p4)[3][4] = new int[2][3][4];
	double** p5;
	double* arrays[2];
	p5 = arrays;
	p5 = new double* [3];*/
}