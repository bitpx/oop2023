#include<iostream>
using  namespace std;

class Point
{
private:
	float x;
	float y;
	const float a;//常数据成员，只能通过初始化列表初始化
	const float& b;//常引用数据成员，只能通过初始化列表初始化
public:
	Point() :x(0), y(0),a(0),b(1)
	{
	}
	Point(float x, float y) :a(0), b(1)
	{
		this->x = x;
		this->y = y;
	}
	//拷贝(只读)构造函数：通过对现有对象数据成员值的拷贝诞生新对象
	Point(const Point& p)//形参必须是引用，可以是常引用
		:x(p.x), y(p.y),a(0), b(1)
	{
		/*this->x = p.x;
		this->y = p.y;*/
		//p.x = 12.2f; 常引用只能读取数据
	}
	void setX(float x)
	{
		this->x = x;
	}
	void setY(float y)
	{
		this->y = y;
	}
	float getX()
	{
		return this->x;
	}
	float getX() const
	{
		return this->x;
	}
	float getY() const //常函数
	{
		return this->y;
	}
};


void main()
{
	Point p1;//非常对象
	cout << p1.getX() << endl;//非常对象调用非常函数
	cout << p1.getY() << endl;//非常对象调用常函数getY()
	const Point p2(1.2f, 2.4f);//声明一个常对象
	cout << p2.getX() << endl;//常对象只能调用常函数
	cout << p2.getY() << endl;//常对象只能调用常函数
	const Point& p3 = p1;//让引用p3成为非常对象p1的别名，在声明时必须初始化
	cout << p3.getX() << endl;//常引用只能调用常函数
	cout << p3.getY() << endl;//常引用只能调用常函数
	const Point& p4 = p2;//让引用p3成为常对象p2的别名，在声明时必须初始化
	cout << p4.getX() << endl;//常引用只能调用常函数
	cout << p4.getY() << endl;//常引用只能调用常函数
}