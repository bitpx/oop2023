#include<iostream>
using namespace std;

class Complex {
private:
	float x, y;
	friend  Complex& operator++(Complex& c);
	friend  Complex& operator++(Complex& c, int);
public:
	friend Complex& operator+ (Complex& a, Complex& b);
	Complex(float x, float y) :x(x), y(y) {}
	float getX() const { return x; }
	float getY()  const { return y; }
	/*Complex& operator+ (Complex& b)
	{
		Complex c(this->x + b.x, this->y + b.y);
		return c;
	}*/
	//前置--
	Complex& operator--()
	{
		this->x -= 1;
		this->y -= 1;
		return *this;
	}
	//后置--
	Complex& operator--(int)
	{
		Complex temp(x, y);
		--(*this);//调用重载好的前置--函数
		return temp;
	}
};

inline Complex& operator+ (Complex& a, Complex& b)
{
	Complex c(a.x + b.x, a.y + b.y);
	return c;
}

//前置++，独立函数版本
Complex& operator++(Complex& c)
{
	c.x += 1;
	c.y += 1;
	return c;
}
//后置++，独立函数版本
Complex& operator++(Complex& c, int)
{
	Complex temp(c.x, c.y);
	++c;//调用重载好的前置++函数
	return temp;
}

ostream& operator<<(ostream& out, const Complex& c)
{
	out << "(" << c.getX() << "," << c.getY() << ")" << endl;
	return out;
}

void main()
{
	Complex a(1, 2), b(3, 4);
	Complex c = a + b;//a.operator+(b);
	cout << c;
	Complex d = ++a;
	cout << a;
	cout << d;
	Complex e = b++;
	cout << b;
	cout << e;
	Complex f = --c;
	cout << c;
	cout << f;
	Complex g = f--;
	cout << f;
	cout << g;
}