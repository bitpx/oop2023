#include<iostream>
using namespace std;

/*
int getMax(int a, int b)
{
	return a > b ? a : b;
}
float getMax(float a, float b)
{
	return a > b ? a : b;
}
double getMax(double a, double b)
{
	return a > b ? a : b;
}
char getMax(char a, char b)
{
	return a > b ? a : b;
}
*/
// 声明函数模板(模具)
template <class T> //定义类型变元T
T getMax(T a, T b)
{
	return a > b ? a : b;
}

void main()
{
	int r1;
	float r2;
	double r3;
	char r4;

	r1 = getMax(1, 2);
	r2 = getMax(2.1f, 2.2f);

