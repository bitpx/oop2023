#include<iostream>
using namespace std;

// 通用的代码块，通用于所有的界面应用
// 具体业务逻辑
inline int add(int x, int y)
{
	return x + y;
}

void main()
{
	int a, b;
	int sum1, sum2;

	// 界面逻辑
	cin >> a >> b;

	// 业务逻辑代码
	sum1 = add(a, b);
	sum2 = add(3, 4);

	// 界面逻辑
	cout << a << " " << b << endl;
}