#include <iostream>
using namespace  std;

class Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Parent(int pri, int pro, int pub) {
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	virtual void show()
	{
		cout << pri << " " << pro << " " << pub << endl;
	}
	void show(int a)
	{
		cout << pri << " " << pro << " " << pub << endl;
		cout << a << endl;
	}
	void show(int a, int b)
	{
		cout << pri << " " << pro << " " << pub << endl;
		cout << a << " " << b << endl;
	}
};
class Son :public Parent {
private:
	int pri;
protected:
	int pro;
public:
	int pub;
	Son(int pri, int pro, int pub)
		:Parent(pri + 2, pro + 2, pub + 2)
	{
		this->pri = pri;
		this->pro = pro;
		this->pub = pub;
	}
	void show()
	{
		cout << pri << " " << pro << " " << pub << endl;

	}
};


int main() {
	Son son(1, 2, 3);
	son.show();
	son.show(1);
	son.show(1, 2);

	return 0;
}
