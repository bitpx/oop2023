#include<iostream>
using namespace std;

class GrandFather
{
public:
	GrandFather(int x)
	{
		cout << "GrandFather对象诞生了！" << endl;
	}
	~GrandFather()
	{
		cout << "GrandFather对象消亡了！" << endl;
	}
};
//虚继承
//GrandFather：虚基类
class Father :virtual public GrandFather
{
public:
	Father() :GrandFather(1)
	{
		cout << "Father对象诞生了！" << endl;
	}
	~Father()
	{
		cout << "Father对象消亡了！" << endl;
	}
};
class Mother :virtual public GrandFather
{
public:
	Mother() :GrandFather(2)
	{
		cout << "Mother对象诞生了！" << endl;
	}
	~Mother()
	{
		cout << "Mother对象消亡了！" << endl;
	}
};

class GirlFriend
{
public:
	GirlFriend(int x)
	{
		cout << "GirlFriend对象诞生了！" << endl;
	}
	~GirlFriend()
	{
		cout << "GirlFriend对象消亡了！" << endl;
	}
};
class FriendGirl
{
public:
	FriendGirl(int a)
	{
		cout << "FriendGirl对象诞生了！" << endl;
	}
	~FriendGirl()
	{
		cout << "FriendGirl对象消亡了！" << endl;
	}
};

class Son :public Mother, public Father {
private:
	GirlFriend o1;
	FriendGirl o2;
public:
	Son() :GrandFather(3), o1(1), o2(2)
	{
		cout << "Son对象诞生了！" << endl;
	}
	~Son()
	{
		cout << "Son对象消亡了！" << endl;
	}
};

void main()
{
	{
		Son son();
	}
}