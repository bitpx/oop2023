#include<iostream> //<>是直接到系统库中搜索
#include"My.h" //"首先查找用户项目文件夹，找不到文件则查找系统库
using namespace std;
using namespace NS;

int i = 0;//命名空间变量
void f2()
{
	i = 10;
	cout << i << endl;
}

class Point {
public:
	Point(float x, float y) { this->x = x; this->y = y; }
	//其他函数省略
private:
	float x, y;
};

void main()
{
	int a = 1, b = 2, sum, max;
	//形参x,y的作用域在圆括号之间
	int add(int x = 0, int y = 0);//函数的原型声明
	int j = 100;

	sum = add(a, b);
	cout << sum << endl;
	cout << j << endl;//局部变量j
	cout << NS::j << endl;//全局变量j
	Clock c(0, 0, 0);
	max = getMax(a, b);
}

void f1(int data)
{
	//用花括号标识独立的代码块
	switch (data)
	{
	case 1:
	{
		int i = 1;
		cout << i;
		break;
	}
	case 2:
	{
		int i = 2;
		cout << i;
		break;
	}
	case 3:
	{
		int i = 3;
		cout << i;
		break;
	}
	default:
		break;
	}
}



//形参x/y的作用域包括函数头的圆括号之间，以及整个函数体
int add(int x = 0, int y = 0)
{
	int z = 3; cout << z;//内部变量z的作用域从声明行开始至函数体结束
	//i为for循环代码块的块级别变量，作用域仅限当前代码块
	for (int i = 0; i < 10; i++)
	{
		cout << i << endl;
	}

	for (int i = 0; i < 10; i++)
	{
		cout << i << endl;
	}

	return x + y + z;
}

