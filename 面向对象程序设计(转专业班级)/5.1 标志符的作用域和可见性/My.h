#pragma once
namespace StusManagerNS {
	namespace ToolsNS {
		class CommonClass {

		};
	}
	namespace UILogicNS {
		class A {

		};
	}
	namespace BLLLogicNS {
		class A {

		};
	}
}
namespace NS {
	namespace NewNS {
		//写代码
	}
	int j = 1;//命名空间变量就是全局变量
	int getMax(int x, int y)
	{
		return x > y ? x : y;
	}
	class Clock {
	private:
		int h, m, s;
	public:
		Clock(int h, int m, int s)
		{
			this->h = h;
			this->m = m;
			this->s = s;
		}
		//其他函数省略
	};
}